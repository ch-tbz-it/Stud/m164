# Auftrag "Mengenlehre"


## Aufgabe 1

Gegeben sind die fünf Mengen: 
A={c,e,z,r,d,g,u,x},
B={c,e,g},
C={r,d,g,t},
D={e,z,u},
E={z,r,u}.

**Beurteilen Sie die folgenden Aussagen**

a) *B ⊂ A* 

b) *C ⊂ A*

c) *E ⊂ A* 

d) *B ⊂ C*

e) *E ⊂ C*



## Aufgabe 2

Gegeben sind die Mengen
A={1;2;3;4;5}
B={2;5}
C={3;5;7;9}

a) Bestimmen Sie A ∩ B

b) Bestimmen Sie A ∪ C

c) Bestimmen Sie B<sup>c</sup>

d) Bestimmen Sie B\C

e) Bestimmen Sie C\B



## Aufgabe 3

Erstellen Sie eigene Beispiele zu den Symbolen ⊂, ∩, ∪ und \.

Verwenden Sie nun aber bezeichnende Mengen, z.B. Nahrung, Videospiele, etc.

---
