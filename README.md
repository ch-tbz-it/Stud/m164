![TBZ Logo](./x_res/tbz_logo.png)
![m319 Picto](./x_res/m164_picto.jpg)



# m164 - Datenbanken erstellen und Daten einfügen

[TOC]

## Lern-Setting in diesem Modul

Einzelne Themen werden durch die Lehrperson eingeführt. Zu Beginn von jedem Block wird ein Recap gemacht, am Ende könne Sie sich anhand der Checkpoints vergewissern, dass Sie die wichtigsten Punkte verstanden haben.

Führen Sie in diesem Modul ein [Lernportfolio](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-Portfolio).

![Selbst-Lern-Zyklus](./x_res/Selbst-Lern-Zyklus_kl.png)
Abb. 1: Selbst-Lern-Zyklus

Organisieren Sie sich während den SOL-Zeiten im Modul anhand des Selbst-Lern-Zyklus mit einem Lernpartner (Tandem). In diesen Modul-Unterlagen wird auf die "Lern-Phasen" verwiesen!
 
<br>

## Einsatz einer KI als Tutor

Grundsätzlich ist nichts gegen den Einsatz einer KI als Tutor (Coach) einzuwenden. Hier die Vorteile, die eine KI im Bildungskontext leisten kann: 

> *DeepSeek Jan-25 / Prompt: "In welcher Phase und wie würdest du als Lernender eine KI einsetzen?"* <br> 
> KI ist ein flexibles und mächtiges Werkzeug, das in allen Phasen des Selbstlernzyklus eingesetzt werden kann. Als Lernender würde ich KI nutzen, um meinen Lernprozess zu **personalisieren**, zu **beschleunigen** und **reflektierter** zu gestalten. Gleichzeitig ist es wichtig, **kritisch mit den Ergebnissen** umzugehen und die KI als unterstützendes Werkzeug zu sehen, **nicht als Ersatz für eigenständiges Denken und Lernen.** <br> 

![KI-Tutor](x_res/KI-Tutor.png)

Als Lernender sollte man den Grundsatz **"Ich will es selber wissen und können!"** haben! Die KI sollte deshalb _nur_ als Tutor eingesetzt werden und nicht als Lösungsmaschine. Geben Sie also nicht die Aufgabenstellung als Prompt ein, sondern lassen Sie sich von der KI in ihrem Denk- und Lösungsprozess unterstützen! Z.B. _Prompt: "Erkläre mir alle Elemente und die Funktionsweisen des Befehls für schnelles und effizientes Importieren von Daten aus einer CSV-Datei!"_
Um beim Prompten die besten Resultate zu erhalten, arbeiten Sie mit den korrekten Fachbegriffen, die sie vorher erlernen. Auch können Sie den KI-Output am besten verifizieren, wenn Sie bereits Grundwissen haben.

Ihre **Übungen bzw. Lernprodukte** sollten also **selber getippt** sein, damit ein Lernprozess stattfinden kann! KI-Ausgaben sollten **gelesen, verstanden, verifiziert und kommentiert** sein. **Quelle** nicht vergessen anzugeben, bei KI: Modell, Datum, Prompt!

### Die KI als Tutor engagieren

Setzen Sie zu Beginn ihrer Lernaktivität folgenden **Prompt** im KI-Chatbot ab. Dieser Prompt engagiert die KI als Tutor mit einem Fokus auf lernprozessrelevante Unterstützung!

**Setup-Prompt:**

```prompt
Sei mein persönlicher Tutor für das Thema "Datenbanken erstellen und Daten einfügen" auf dem Niveau der Schweizer Berufsschule für Informatik. 
Deine Aufgabe ist es, mir beim Verständnis der Grundlagen und Konzepte zu helfen, ohne direkt Lösungen für meine Aufgaben zu liefern. 
Führe mich stattdessen durch den Lernprozess, indem du mir zeigst, wie ich Probleme selbstständig angehen kann. 

Halte dich dabei an den Selbstlernzyklus, der in der folgenden Website erklärt wird: <https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-Portfolio> 


Strukturiere deine Antworten immer in die folgenden vier Kategorien: 

1) Grundlagen: Erkläre mir kurz und verständlich die Grundlagen, die für die Frage relevant sind. 
2) Konzeptverständnis: Hilf mir, das Prinzip oder die Logik hinter der Frage zu verstehen. 
3) Nächster Schritt: Zeige mir, wie ich den nächsten Schritt in meinem Lernprozess machen kann, um zur Lösung zu gelangen. 
4) Nutzung Lernportfolio: Gib mir einen Vorschlag, wie ich dazu einen nützlichen Eintrag im Lernportfolio machen kann.

Benutze immer nur die Website <https://gitlab.com/ch-tbz-it/Stud/m164> mit den Unterordnern, um den Kontext des Themas zu erfassen. 
Deine Antworten sollen sich immer auf die "MariaDB Version 10" beziehen.

Bitte halte dich strikt an diese Struktur und das angegebene Niveau.
```

 

---
---

 
## Lektionenplan mit Links (Siehe auch Klassenordner)

| Tag  |  Themen | Inhalt | [Modul ID <br> Kompetenzmatrix](https://gitlab.com/modulentwicklungzh/cluster-data/m164/-/blob/master/1_Kompetenzmatrix/README.md) | 
|:----:|:------ |:-------------- |:---:|
|  1   | [Intro <br> Recap <br> Installation lokales DBMS](./1.Tag/README.md)  | Einführung Klasse ins Modul <br> Auffrischen Fachbegriffe, Konzepte, Normalisieren <br> MySQL DBMS installieren: MySQL Dienst & Klient | A1 |
|  2   | [ERD erweitern <br> <br> DBMS <br> Datenbasis erstellen](./2.Tag/README.md) | Generalisierung / Spezialisierung  <br> Indentifying / Non-Identifying relationship <br> Workbench Forward Engineering <br> &#8594; DDL (CHARSET) | A1 B1 |
|  3   | [Datentypen, Mehrfachbeziehungen, Rekursion <br> Datenbasis bearbeiten ](./3.Tag/README.md) | Datentypen <br> Spezielle Beziehungen <br> DML | B2 B3|
|  4   | **LB1 Tag 1 - 3 (50%)**  <br>  [ Ref. Integrität / FK-Constraints](./4.Tag/README.md) | Beziehungen und Constraints  <br>  Mengenlehre <br> DQL (JOIN) | B2 B3 <br> C1 C3 <br> D1|
|  5   | **[NP LB1 Tag 1 - 3]**  <br> [Datenintegrität <br> DQL ](./5.Tag/README.md) | FK-Constraint-Option Regeln <br> SELECT ... (**W**arum **g**eht **H**erbert **o**ft **l**aufen?) | C1 C3 D1 |
|  6   | [SUBQUERY <br> Bulkimport](./6.Tag/README.md) | Sub-SELECT <br> LOAD DATA INFILE |  C2 C3 |
|  7   | [Backup <br> Daten  normalisiert einbinden](./7.Tag/README.md) | Dump & Backuptool <br> Daten in normalisierte DB importieren | C2 |
|  8   | [**Praxisarbeit**](./8.Tag/README.md) | CSV-Data &rarr; DB-3.NF <br> OpenData   | A1 B1 C2 |
|  (9)   | [Reserve CTE & Stored Procedures](./9.Tag/README.md) |  <br>          | D1 (&rarr; M141) |
|  9 (10)   | Repetition / Vorbereitung Prüfung <br> **LB2 Tag 4 - 8 (50%)**|  <br>               

---

**Info Lernziele LBs:**

**LB1 Tag 1 - 3 (50%, 75min): Recap M162, DBMS, General/Spezial, Non/Identifying, DDL, DML, DQL**  <br> 30min Theorie ECOLM mit selbstgeschriebenem, ausgedrucktem Spick (max.5 x A4), 40min Praxis Openbook mit DBMS

**LB2 Tag 1 - 8 (50%, 110min):** <br> 30min Theorie (⅓) ECOLM mit selbstgeschriebenem, ausgedrucktem Spick (max.5 x A4): Mengenlehre, Konsistenz und ref.Integrität, Beziehunges-Constraints, Aggregationsfunktionen, Bulk-Import & Datensicherung <br> 80min Praxis (⅔) Openbook: Daten in 3-NF Datenbasis einbinden, auslesen und backuppen

**Optional**: *Bewertung Lernportfolio (Link vorab abgeben)*

---

![](./x_res/Buch.jpg)

# Referenz zu den Unterlagen, Aufträgen und Vorgaben


1. [Installation Tools (DBMS & Clients)](./1.Tag/Installation_SW.md)
2. [Datenbasen](./Daten)
