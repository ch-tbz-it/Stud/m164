# DML (Data Manipulation Language)

[TOC]

---

## INSERT
Der INSERT-Befehl in MySQL wird verwendet, um neue Zeilen in eine vorhandene Tabelle einzufügen. Mit diesem Befehl können Sie Daten in die Tabelle einfügen, indem Sie Werte für jede Spalte angeben, oder Sie können nur Werte für bestimmte Spalten angeben.

Ein INSERT-Statement kann in [**Kurz- oder Langform**](insert-into.pdf) geschrieben werden.

Die nachfolgenden Befehle basieren auf Langform INSERT-Statement.
Die Syntax für die Verwendung von INSERT lautet wie folgt:

```sql
INSERT INTO table_name (column1, column2, column3, ...) VALUES (value1, value2, value3, ...);
```
"table_name" ist der Name der Tabelle, in die Daten eingefügt werden sollen, und "column1", "column2", "column3", ... sind die Spaltennamen, in die Daten eingefügt werden sollen. "value1", "value2", "value3", ... sind die Werte, die in die Spalten eingefügt werden sollen.

Hier ist ein Beispiel für die Verwendung von INSERT:

```sql
INSERT INTO customers (name, email, address) VALUES ('John Smith', 'john@example.com', '123 Main St');
```
Dieser Befehl fügt eine neue Zeile in die Tabelle "customers" ein und füllt die Spalten "name", "email" und "address" mit den Werten "John Smith", "john@example.com" und "123 Main St".

Es ist auch möglich, den INSERT-Befehl mit einer SELECT-Abfrage zu kombinieren, um Daten aus einer anderen Tabelle einzufügen. Hier ist ein Beispiel dafür:

```sql
INSERT INTO customers (name, email, address)
SELECT name, email, address
FROM new_customers;
```

Dieser Befehl fügt neue Zeilen in die Tabelle "customers" ein und füllt die Spalten "name", "email" und "address" mit den Werten aus der Tabelle "new_customers".
Der SELECT-Statement wird im Kapitel [DQL](../3.Tag/DQL_SEL.md) detailliert erklärt.

Es gibt viele Optionen und Möglichkeiten, die mit dem INSERT-Befehl in MySQL möglich sind, aber das grundlegende Konzept ist, dass Daten in eine Tabelle eingefügt werden, indem Werte für jede Spalte angegeben werden.


## UPDATE
Der UPDATE-Befehl in MySQL wird verwendet, um bestehende Datensätze in einer Tabelle zu aktualisieren. Der Befehl hat folgende Syntax:

```sql
UPDATE table_name
SET column1 = value1, column2 = value2, ...
WHERE condition;
```
Hierbei wird table_name durch den Namen der Tabelle ersetzt, die aktualisiert werden soll. column1 = value1, column2 = value2, ... gibt an, welche Spalten in der Tabelle aktualisiert werden sollen und welche Werte sie erhalten sollen. Die WHERE-Klausel gibt an, welche Datensätze aktualisiert werden sollen. Wenn keine WHERE-Klausel angegeben wird, werden alle Datensätze in der Tabelle aktualisiert.

Beispiel:

Angenommen, Sie haben eine Tabelle "customers" mit den Spalten "customer_id", "first_name", "last_name" und "email". Sie möchten den Vornamen des Kunden mit der ID 1 in "John" ändern. Der UPDATE-Befehl würde wie folgt aussehen:

```sql
UPDATE customers
SET first_name = 'John'
WHERE customer_id = 1;
```
Dadurch wird der Vorname des Kunden mit der ID 1 in "John" geändert.

> Hinweis: Bei Fehlercode 1175 müssen Sie folgende Einstellung auschalten: <br>
> ![Safe Updates](./media/Einstellung_Safe_Updates.png)

## DELETE
Der DELETE-Befehl in MySQL wird verwendet, um Datensätze aus einer Tabelle zu löschen. Der Befehl hat folgende Syntax:

```sql
DELETE FROM table_name
WHERE condition;
```
Hierbei wird table_name durch den Namen der Tabelle ersetzt, aus der Datensätze gelöscht werden sollen. Die WHERE-Klausel gibt an, welche Datensätze gelöscht werden sollen. Wenn keine WHERE-Klausel angegeben wird, werden alle Datensätze in der Tabelle gelöscht.

Beispiel:

Angenommen, Sie haben eine Tabelle "customers" mit den Spalten "customer_id", "first_name", "last_name" und "email". Sie möchten den Kunden mit der ID 1 aus der Tabelle löschen. Der DELETE-Befehl würde wie folgt aussehen:

```sql
DELETE FROM customers
WHERE customer_id = 1;
```
Dadurch wird der Datensatz des Kunden mit der ID 1 aus der Tabelle gelöscht. Beachten Sie jedoch, dass durch das Löschen von Datensätzen Informationen verloren gehen können und dass Sie daher sicherstellen sollten, dass Sie die richtigen Datensätze löschen, bevor Sie den DELETE-Befehl ausführen.

---

**Quellen**:

- <https://dev.mysql.com/doc/refman/8.0/en/insert.html>

- <https://dev.mysql.com/doc/refman/8.0/en/update.html>

- <https://dev.mysql.com/doc/refman/8.0/en/delete.html>