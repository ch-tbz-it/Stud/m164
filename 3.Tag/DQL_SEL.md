# DQL (Data Query Language)

## SELECT

Der SELECT-Befehl in MySQL wird verwendet, um Daten aus einer oder mehreren Tabellen abzurufen. Mit diesem Befehl können Sie bestimmte Spalten abrufen, Filterbedingungen anwenden, Daten sortieren und vieles mehr.

Die Syntax für die Verwendung eines einfachen SELECT lautet wie folgt:

```sql
SELECT column1, column2, ... FROM table_name WHERE condition ORDER BY column1, column2, ...;
```
"column1", "column2", ... sind die Spalten, die abgerufen werden sollen. Wenn Sie alle Spalten in der Tabelle abrufen möchten, können Sie den *-Operator verwenden. "table_name" ist der Name der Tabelle, aus der Daten abgerufen werden sollen. "condition" ist eine optionale Bedingung, die die zurückgegebenen Zeilen einschränkt. "ORDER BY column1, column2, ..." ist eine optionale Klausel, die die zurückgegebenen Zeilen sortiert.

### WHERE Bedingungen
| Operator | Erklärung |
|---|---|
| = | Genau gleich (Achtung: % nicht möglich!) <br> - Text: GROSS/klein wird nicht unterschieden!|
| LIKE <br> NOT LIKE | wie '=',  aber Wildcard % und _ sind möglich! <br> Auschluss von Werten|
| > | Grösser als |
| < | Grösser als |
| >= | Grösser oder gleich wie |
| <= | Kleiner oder gleich wie |
| <> (!=) <br> NOT | Nicht gleich  <br> NICHT: Nachfolgende Bedingung wird negiert |
| AND <br> OR | UND: Alle Bedingungen müssen wahr sein  <br> OR: Mind. der Bedingungen muss wahr sein <br> - Achtung: Evtl. Klammern richtig setzen! |
| .. BETWEEN .. AND .. <br> salery BETWEEN 100 AND 999 | Zwischen zwei Werten (inklusive) <br> - Bsp.: (salery >= 100 AND salery <= 999)  |
| IN ( .. , .. ) <br> NOT IN ( .. ) | Mehrere mögliche Werte (siehe auch Subselect, ANY, SOME) <br> Ausschluss von Werten|
| IS **NULL** <br> IS NOT **NULL** | Feld (nicht) leer? |


Hier ist ein Beispiel für die Verwendung von SELECT:

```sql
SELECT first_name, last_name, email, hire_date, salary FROM employees WHERE salary > 50000 ORDER BY hire_date;
```
Dieser Befehl gibt die Spalten "first_name", "last_name", "email", "hire_date" und "salary" für alle Mitarbeiter zurück, deren Gehalt größer als 50000 ist, und sortiert die Ergebnisse nach dem Einstellungsdatum.

Es gibt viele Optionen und Möglichkeiten, die mit dem SELECT-Befehl in MySQL möglich sind, einschließlich der Verwendung von Funktionen wie AVG, COUNT, MAX und MIN, der Verwendung von JOINs, um Daten aus mehreren Tabellen abzurufen, und vielem mehr. Der SELECT-Befehl ist ein sehr leistungsfähiges Werkzeug für die Abfrage von Daten in einer SQL-Datenbank.

>Das Dokument [select.pdf](select.pdf) liefert Ihnen detaillierte Erklärungen und praktische Beispiele sowie kleine Übungen aus der [filmeDatenbank](../Daten/filmeDatenbank_dump.sql).

