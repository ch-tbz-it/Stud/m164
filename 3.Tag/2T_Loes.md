![](../x_res/tbz_logo.png)

# M164 Lösungen 2.Tag


![Checkpoint](../x_res/CP.png)


### Generalisierung/Spezialisierung: Warum macht man das?

- Bsp: Fahrzeug - (Auto / Motorrad), Book - (E-Book, Paper-Book)
- Um redundante Informationen in der Generalisierungs-Entität zusammenzufassen. 

### Identifying Beziehung! Warum macht man das?

- Sie drückt aus, dass die Detail-Tabelle nicht ohne die Master-Tabelle existieren kann. Dient zur Klarheit beim Design und hat Wirkung, wenn einzelne DS aus den Tabellen gelöscht werden sollen!

### Wie wird eine Identifying Beziehung mit SQL-Befehl(en) erstellt? 

![ID + FK = UNIQUE](./media/Identifying_SQL.png)

### Welche Befehle gehören zur DDL-Gruppe?

- CREATE SCHEMA/DATABASE/TABLE/INDEX/VIEW/...
- ALTER TABLE DROP/ADD/MODIFY/RENAME/CHANGE/...
- DROP TABLEDATABASE/INDEX/...
- RENAME TABLE
- TRUNCATE TABLE
