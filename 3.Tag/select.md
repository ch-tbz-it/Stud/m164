# Select

Zeit: 30 Min.<br>
Form: 2er Team

Gegeben ist die [Filme Datenbank](../Daten/filmeDatenbank_dump.sql).

## Aufgaben

Setzen Sie diese Aufgabe um: 

a.	Lassen Sie sich mit SELECT alle Datensätze Ihrer DVD-Sammlung ausgeben.
b.	Erstellen Sie eine Anweisung, die alle Filmtitel und die jeweils zugehörige Nummer ausgibt.

c.	Erstellen Sie eine Anweisung, die alle Filmtitel und den jeweils zugehörigen Regisseur ausgibt.

d.	Erstellen Sie eine Liste mit allen Filmen von Quentin Tarantino.

e.	Erstellen Sie eine Liste mit allen Filmen von Steven Spielberg.

f.	Erstellen Sie eine Liste aller Filme, in denen der Regisseur den Vornamen "Steven" hat.

g.	Erstellen Sie eine Liste mit allen Filmen, die länger als 2 Stunden sind.

h.	Erstellen Sie eine Liste mit allen Filmen, die von Tarantino oder von Spielberg gedreht wurden.

i.	Suchen Sie alle Filme heraus, die von Tarantino gedreht wurden und kürzer als 90 Minuten sind.

j.	Sie erinnern sich an einen Film, in dessen Titel "Sibirien" vorkam. Suchen Sie ihn.

k.	Lassen Sie sich alle Teile von "Das große Rennen" ausgeben.

l.	Lassen Sie sich eine Liste aller Filme ausgeben, sortiert nach Regisseur.

m.	Lassen Sie sich eine Liste aller Filme ausgeben, sortiert nach Regisseur, dann nach Filmtitel.

n.	Lassen Sie sich eine Liste aller Filme von Tarantino ausgeben, die längsten zuerst.
