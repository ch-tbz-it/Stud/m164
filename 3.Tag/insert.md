# Insert

Zeit: 30 Min.<br>
Form: 2er Team

Gegeben sei folgende Tabelle:

```sql
CREATE TABLE kunden (
    kunde_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, 
    vorname VARCHAR(255), 
    nachname VARCHAR(255), 
    land_id INT, 
    wohnort VARCHAR(255))
```


## Aufgaben
1.	Setzen Sie diese Aufgabe um: <br>
    
    a.	Legen Sie mit der Kurzform einen Kunden mit folgenden Daten an: Heinrich Schmitt aus Zürich, Schweiz (Schweiz hat die land_id 2).

    b.	Legen Sie mit der Kurzform eine Kundin mit folgenden Daten an: Sabine Müller aus Bern, Schweiz (Schweiz hat die land_id 2).
    
    c.	Legen Sie mit der Kurzform einen Kunden mit folgenden Daten an: Markus Mustermann aus Wien, Österreich (Österreich hat die land_id 1).
    
    d.	Legen Sie mit der Langform einen Kunden mit folgenden Daten an: Herr Maier.
    
    e.	Legen Sie mit der Langform einen Kunden mit folgenden Daten an: Herr Bulgur aus Sirnach.
    
    f.	Legen Sie mit der Langform einen Kunden mit folgenden Daten an: Maria Manta.

2.	Die folgenden Anweisungen enthalten Fehler. Finden und korrigieren Sie die Fehler. Probieren Sie die Funktionsfähigkeit der Befehle an der obigen Tabelle aus.
    
    a.	
    ```sql
    INSERT INTO (nachname, wohnort, land_id) VALUES ('Fesenkampp', 'Duis-burg', 3);
    ```

    b.	
    ```sql
    INSERT INTO kunden ('vorname') VALUES ('Herbert');
    ```

    c.	
    ```sql
    INSERT INTO kunden (nachname, vorname, wohnort, land_id) VALUES ('Schulter', 'Albert', 'Duisburg', 'Deutschland');
    ```

    d.	
    ```sql
    INSERT INTO kunden ('', 'Brunhild', 'Sulcher', 1, 'Süderstade');
    ```

    e.	
    ```sql
    INSERT INTO kunden VALUES ('Jochen', 'Schmied', 2, 'Solingen');
    ```

    f.	
    ```sql
    INSERT INTO kunden VALUES ('', 'Doppelbrecher', 2, '');
    ```

    g.	
    ```sql
    INSERT INTO kunden (nachname, wohnort, land_id) VALUES ('Christoph', 'Fesenkampp', 'Duisburg', 3);
    ```

    h.	
    ```sql
    INSERT INTO kunden (vorname) VALUES ('Herbert');
    ```

    i.	
    ```sql
    INSERT INTO kunden (nachname, vorname, wohnort, land_id) VALUES (Schulter, Albert, Duisburg, 1);
    ```

    j.	
    ```sql
    INSERT INTO kunden VALUE ('', "Brunhild", "Sulcher", 1, "Süderstade");
    ```

    k.	
    ```sql
    INSERT INTO kunden VALUE ('', 'Jochen', 'Schmied', 2, Solingen);
    ```
