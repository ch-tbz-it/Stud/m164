![TBZ Logo](../x_res/tbz_logo.png)√
![m164 Picto](../x_res/m164_picto.jpg)



# m164 - Datenbanken erstellen und Daten einfügen

[TOC]

---

# Tag 3

> ![Teacher](../x_res/Teacher.png) <br> Recap / Q&A Tag 2: Genealisierung/Spezialisierung, Indentifying Relationships <br> <br> ![Tourenplaner mit Generalisierung](./media/TP_Gen.png) << In Workbench fertigstellen!
>  <br>
> [Lösung 2.Tag](2T_Loes.md)

# Datentypen

>![Teacher](../x_res/Teacher.png) <br> Repetition M162 [Datentypen](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Daten_Formate/Datentypen.md)


### Auftrag 

![ToDo](../x_res/ToDo.png) *Zeit: ca. 35 Min*

![Learn](../x_res/Train_R1.png)

**Tabelle Datentypen**


| Datentyp | MariaDB (MySQL) | Beispiel | Bemerkung / Einstellungen |
|----|----|----|----| 
| Ganze Zahlen |  |  |  |
| Natürliche Zahlen |  |  |  |
| Festkommazahlen <br> (Dezimalzahlen) | Decimal(M[,D]) | Decimal(6,2) <br> 1234.56 | M=Gesamte Anzahl Stellen <br> D=Nachkommastellen|
| Aufzählungstypen |  |  |  |
| Boolean (logische Werte) |  |  |  |
| Zeichen (einzelnes Zeichen) |  |  |  |
| Gleitkommazahlen |  |  |  |
| Zeichenkette fester Länge |  |  |  |
| Zeichenkette variabler Länge |  |  |  |
| Datum und/oder Zeit |  |  |  |
| Zeitstempel| | |  |
| Binäre Datenobjekte <br> variabler Länge (z.B. Bild) |  |  |  |
| Verbund |  |  |  |
| JSON |  |  |  |	


  1. Vervollständigen Sie die obige Tabelle der Datentypen der DBMS [MariaDB](https://mariadb.com/kb/en/data-types/) und/oder [MySQL](https://dev.mysql.com/doc/refman/8.0/en/data-types.html).

  ![Note](../x_res/note.png) Ablage im Lernportfolio (Scripte und Resultate)

<br><br><br>

---

![Learn](../x_res/Learn.png)

# (1) Mehrfachbeziehungen

Bei Mehrfachbeziehungen haben zwei Tabellen mehrere Beziehungen, die unabhängig voneinander sind und jeweils einen anderen Sachverhalt repräsentieren. Um die verschiedenen Beziehungen eindeutig zu kennzeichnen, müssen sie möglichst aussagekräftig beschriftet werden (Rolle). Die Kardinalitäten können natürlich verschieden sein, da die Beziehungen voneinander unabhängig sind. In den Beispielen unten gibt es drei unabhängige Beziehungen zwischen den Entitätstypen *tbl_Fahrten* und *tbl_Orte*. Eine dieser Beziehungen ist die Auflistung der Orte, die bei einer Tour angefahren werden können. Dies ist eine mc:mc-Beziehung und benötigt daher eine Transformationstabelle.

![](./media/Mehrfach_Beziehungen.png)


---

# (2) Rekursion (strenge Hierarchie)

Bis jetzt haben wir Beziehungen zwischen zwei verschiedenen Entitätstypen betrachtet. Es gibt aber auch die Möglichkeit, dass an einer Assoziation nur ein einziger Entitätstyp beteiligt ist. Konkret bedeutet das, dass ein Datensatz dieser Tabelle mit einem anderen Datensatz aus der gleichen Tabelle in Beziehung steht. Eine reine Hierarchie können wir innerhalb der gleichen Tabelle erledigen. Sie erhält also einen Fremdschlüssel, der auf den Identifikationsschlüssel der eigenen Tabelle verweist. Das klassische Beispiel hierfür ist eine Firmenorganisation, bei der jede Person genau eine andere Person als Vorgesetzten hat. Da die in der Hierarchie höchste Person natürlich keinen Vorgesetzten mehr haben kann, ist es eine c:mc Beziehung. **Würden wir dieses Fremdschlüsselattribut auf „Not Null“ belassen (= 1:mc), könnten wir die „höchste Person" nicht erfassen.**

![](./media/Rekursion.png)

Abbildung: Darstellung einer einfachen Hierarchie in der Workbench

Mit dieser einfachen Variante kommen wir aber nicht weiter, wenn aus der klaren Hierarchie (einmal-Rekursion – nur ein Vorgesetzter ist möglich) eine Netzwerkstruktur wird: In Fall bedeutet das, dass eine Person auch mehrere Vorgesetzte haben können sollte, was bei der Rekursion nicht geht. 

# (3) Einfache Hierarchie (mit Zwischentabelle)

Bei einer Netzwerkstruktur handelt es sich um eine mc:mc Beziehung und wir benötigen eine Transformationstabelle. Speziell daran ist, dass die beiden Fremdschlüssel der Transformationstabelle jeweils auf einen Identifikationsschlüssel der gleichen Tabelle zeigen, aber in unterschiedlichen Rollen. Im Beispiel sind das die Rollen „ist_Vorgesetzter_von“ und „ist_Mitarbeiter_von“.

![](./media/Einfache_Hierarchie.png)

Abbildung: Hierarchie-Darstellung einer netzwerkförmigen Struktur

Einfache Hierarchien können mit Rekursionen innerhalb der gleichen Tabelle abgebildet werden. Solange jedes Element nur maximal ein Oberelement hat, reicht ein Fremdschlüssel, der auf die Identifikationsschlüssel der eigenen Tabelle verweist. Lediglich das Wurzelelement oder Topelement hat einen NULL-Wert, da die Hierarchie ja irgendwo enden (oder anfangen) muss. Es handelt sich hierbei um eine c:mc Beziehung.

Wenn aber mehrere Oberelemente zugelassen sind (z. B. mehrere Projektleiter), dann handelt es sich um eine mc:mc-Beziehung und es wird eine Transformationstabelle benötigt, die für jeden Beziehungsgraphen einen Datensatz enthält, der angibt, welches Oberelement zu welchem Unterelement gehört.

# (4) Stücklistenproblem

Eine klassische Anwendung der Rekursion in der Datenmodellierung ist die Stücklistenproblematik. Es gibt eine Tabelle mit Produkten und deren Eigenschaften. Jetzt kann sich aber auch ein Produkt aus mehreren anderen Produkten zusammensetzen (modulare Produkte). Beides – die zusammengesetzten Produkte (Aggregate) sowie die einzelnen Bestandteile – befindet sich in unserer Produkte- oder Artikeltabelle und wird auch einzeln verkauft. Das Problem besteht nun darin, eine Liste mit Einzelteilen zu erzeugen, die alle Artikel auf elementarer Ebene enthält (die also nicht mehr aus mehreren Produktteilen bestehen).

Ein gutes Beispiel – sogar mit SQL-Code – finden Sie hier:

<https://infocenter.sybase.com/help/index.jsp?topic=/com.sybase.help.sqlanywhere.12.0.1/dbusage/parts-explosion-cte-sqlug.html>

![Graph representing hierarchical relationship of bookshelf subcomponents](./media/a23bd48e09ca11406474760f63e8d173.png)

Abbildung: Das Beispiel beschreibt die Artikel einer Elementmöbelfabrik.

Lösung mit Rekursion: Da es sich im Modell ja nicht um eine einfache Hierarchie handelt (c:mc), reicht die Erstellung eines Fremdschlüsselattributs in der Produktetabelle nicht aus. Neben der Produktetabelle benötigen wir eine weitere Tabelle, die die Zusammensetzungen enthält. Also, welche Komponente (Einzelteil oder Aggregat) fliesst in welches Aggregat und mit welcher Menge ein?

---

### Auftrag - Obige Beziehungen (1) - (3) in Tourenplaner einbauen

![ToDo](../x_res/ToDo.png) *Zeit: ca. 25 Min*

![Learn](../x_res/Train_R1.png)

1. Falls Sie ihren Tourenplaner noch nicht mit der **Generalisierung** tbl_Mitarbeiter ausgebaut haben, führen Sie dies Aktion zuerst aus ...
2. Bauen Sie die obigen Beziehungen **Mehrfachbeziehungen** (1) in Ihr Tourenplaner-Schema ein.
3.  Bauen Sie auch *beide* Beziehungen **Rekursion** (2) *und* **einfache Hierarchie** (3) ein. <br> ![Herarchien](./media/Hierarchien.png)
4. Erstellen Sie die Struktur in der Datenbasis auf ihrem MySQL-Server: Mit **Forward Engineering**. <br> Wir brauchen diese Struktur in der letzen Aufgabe wieder ...
5. Studieren Sie das **Stücklistenproblem** (4) und tauschen Sie mit ihrem(r) SitznachbarIn darüber aus: Wie ist die Tabelle "component and subcomponent" aufgebaut? 

![Note](../x_res/note.png) Ablage im Lernportfolio (Scripte und Resultate)

<br><br><br>

---

# Datenbearbeitung der Datenbasis (Repetition DML ÜK Modul 106)

![Learn](../x_res/Learn.png)

Nun wollen wir Daten in die Struktur einfügen und bearbeiten.

![Konzept](./media/Konzept_DML.png)

> Für diejenigen, die den ÜK noch nicht hatten: [INSERT Präsentation](./insert-intro.pdf) und [DML_Befehle](./DML_Intro.md). Hier noch eine [UPDATE Präsentation](./update-alter-delete-drop.pdf) zum zweiten Auftrag.

### Aufträge DML

![ToDo](../x_res/ToDo.png) *Zeit: ca. 35 Min*

![Learn](../x_res/Train_R1.png)

1. Setzen Sie den [Auftrag Insert](insert.md) um.
2. Setzen Sie den [Auftrag update delete alter drop](update_delete_alter_drop.md) um.

![Note](../x_res/note.png) Ablage im Lernportfolio (Scripte und Resultate)
 
 
<br><br><br>

---

# Daten auslesen (Repetition ÜK Modul 106)

![Learn](../x_res/Learn.png)

Der SQL-Befehl Select gehört je nach Einteilung zur Gruppe DML oder zu der eigenen Gruppe DQL (Data-Query-Language).


**Studieren** Sie die ausführliche **Syntax** des Select-Befehls:

[MariaDB](https://mariadb.com/kb/en/select/), [MySQL](https://dev.mysql.com/doc/refman/8.0/en/select.html) <br> und allenfalls diese [Präsentation](select.pdf) ( = Repetition ÜK M106 )!

> Hinweis: Mit Select können auch die internen Funktionen ausgeführt werden! *(siehe Script Beispiele)*

Der SELECT-Befehl:

![SELECT Konzept](./media/Select_Konzept.png)

```SQL
-- Select Beispiele:
-- -----------------

-- SELECT Wertet aus und zeigt in angeschriebenen Spalten an
SET @var = 12;               -- User Var setzen
SELECT @var as 'Var Inhalt'; 
SELECT 23647 / 2 AS Division, 214649 * 2 AS Multiplikation, 12+123.3 AS Addition;
SELECT IF(0 = FALSE, 'true', 'false') AS Logik, IF(1 = TRUE, 'true', 'false') AS Logik ;
SELECT ROUND(1234.5,0) AS Ganze, ROUND(12345.5678,2) AS Rappen, ROUND(1234.5,-2) AS Hundert; -- Funktion Runden
SELECT pi() AS PI, SIN(90) AS Sinus, POWER(2,3) AS '2^3', ABS(-1.5) AS 'Ohne -';             -- Mathe Funktion 


-- Ausgabe ALLER Attribute (*) einer Tabelle 
SELECT * FROM filmedatenbank.dvd_sammlung;   
-- auch ohne  "filmedatenbank."  wenn vorher "USE filmedatenbank" verwendet wurde!

USE filmedatenbank;

-- SELECT * : Horizontale Einschränkuung
SELECT film FROM filmedatenbank.dvd_sammlung;
SELECT film, regisseur FROM filmedatenbank.dvd_sammlung;
SELECT id, film, nummer, laenge_minuten, regisseur FROM dvd_sammlung;

-- WHERE: Vertikale Einschränkung
SELECT * FROM dvd_sammlung WHERE film = 'Angst';

-- o findet auch ö's   'ö' = 'oe'
Select * FROM dvd_sammlung WHERE regisseur like '%Tar%' AND ( film = '%a%' OR film like '%o%');
```


### Auftrag Select

![ToDo](../x_res/ToDo.png) *Zeit: 30 Min., Form: 2er Team*

![Learn](../x_res/Train_R1.png)

1. Setzen Sie den [Auftrag Select](select.md) um.

![Note](../x_res/note.png) Ablage im Lernportfolio (Scripte und Resultate)

---

## Auftrag - Erweiterter Tourenplaner mit Daten füllen


![ToDo](../x_res/ToDo.png) *Zeit: 45 Min., Einzelarbeit*

![Learn](../x_res/Train_D1.png)

Verwenden Sie nun das oben erstellte, **neue Tourenplaner-Schema** mit den neuen Beziehungen:

Wir wollen nun folgende Daten einpflegen:

**tbl_Mitarbeiter**: (Generalisierung!)

| ID | Vorname | Name | Telefonnummer |
|:--:|---------|------|---------------|
| 1 | Hans | Muster | +41 76 764 23 23|
| 2 | Theo | Dohr | +41 79 324 55 78 |
| 3 | Justin | Biber | +41 79 872 12 32 |
| 4 | Johann S. | Fluss | +41 79 298 98 76 |
| 5 | Diana | Knecht | +41 78 323 77 00 |
| 6 | Anna | Schöni | +41 76 569 67 80 |
| **8** | Lucy | Schmidt| +49 420 232 2232 |
| 9 | Ardit | Azubi|  |



### Hierarchie(n) 

Bei *Rekursion*- **und** bei *Hierarchie*-Beziehungen eintragen! 

**Organigramm**:

- Firmeninhaber: *Justin Biber*
- Chef Dispo: *Anna Schöni*
- Chef Fahrer: *Theo Dohr*
- DisponentInnen: *Schöni, Schmidt, Muster*
- FahrerInnen: *Knecht, Fluss, Dohr, Muster*

Achten Sie darauf, dass Sie mit dem SQL Befehl `UPDATE SET WHERE IN` arbeiten:

```SQL
-- Hierarchie Rekursion:
UPDATE tbl_Mitarbeiter
  SET FS_Vorgesetzter = 'ID_des_Vorgesetzten'
  WHERE ID_Mitarbeiter IN ('ID_des_Mitarbeiters1', 'ID_des_Mitarbeiters2');
```

```SQL
-- Etwas komfortabler:
UPDATE tbl_Mitarbeiter
SET FS_Vorgesetzter = (
    SELECT ID_Mitarbeiter
    FROM tbl_Mitarbeiter
    WHERE Vorname = 'Vorgesetzter_Vorname' AND Nachname = 'Vorgesetzter_Nachname'
)
WHERE ID_Mitarbeiter IN (
    SELECT ID_Mitarbeiter
    FROM tbl_Mitarbeiter
    WHERE Vorname IN ('Mitarbeiter1_Vorname', 'Mitarbeiter2_Vorname', ...)
    AND Nachname IN ('Mitarbeiter1_Nachname', 'Mitarbeiter2_Nachname', ...)
);
```
> Hinweis: Bei der Erstellung der Hierarchie mit einer Transformationstabelle müssen Sie sich im Klaren sein, was ein `NULL`-Wert im `FS_...` bedeutet! (Ein Eintrag in der `tbl_Hierarchie` entspricht einer konkreten Beziehung, und darf deshalb kein `NULL` enthalten! D.h. eine Beziehung, die nicht existiert, erscheint auch nicht in dieser Zwischentabelle.)

**tbl_Hierarchie**: 

| FK\_ist\_Vorg\_von| FK\_ist\_MA\_von  | Bemerkung |
|---|---|---|
| 3 | 2 | Biber ist Chef von Dohr |
| 2 | 5 | Dohr ist Chef von Knecht |
| **NULL** | 9 | nicht zulässig |
| .. | .. | usw. |



### Mehrfachbeziehungen

Pflegen Sie die folgenden Touren in Ihre Datenbasis ein. Die Disponentin ist dabei immer Anna Schöni. 

| Tour-Nummer | Start-Ort | Ziel-Ort |  Via |
|:-------:|----------|---------|-----|
| 12 | HB, 8000 Zürich | HB, 4000 Basel |   Flughafen, 8309 Kloten <br> Bahnhofstr.1 , 4410 Liestal | 
| 32 | Schulhausstr.23, 8400 Winterthur | HB, 8000 Zürich |   Schaffhauserstr. 123, 8400 Winterthur <br> Eggweg 45, 8400 Winterthur | 
| 45 | HB, 8001 Zürich | HB, D-70180 Stuttgart |    HB, D-78224 Singen | 
| 51| Ausstellungsstr. 80, 8090 Zürich | Flughafen, 1200 Genf |   Direkt| 

*Anmerkung: Evtl. Strasse weglassen, wenn kein entspr. Attribut erstellt wurde.*

### Testen: Überprüfen mit Select

- Lesen Sie diese obig eingefügten Daten wieder aus und überprüfen Sie deren Richtigkeit! <br> Formatieren Sie die Ausgaben so, dass Sie den obigen Tabellen entsprechen. <br> *(Verwenden Sie SELECT JOIN dazu, um obige Hierarchien und Fahrten darstellen zu können.)*

<br>

![Note](../x_res/note.png) Ablage im Lernportfolio (Scripte und Resultate)


---

![Checkpoint](../x_res/CP.png)

## Checkpoint

- Welche Schwierigkeiten beim Einfügen von Daten ergeben sich, wenn z.B. der `FK_Vorgesetzer` als `Constraint` definiert ist? *(Tipp: ref. Integrität)*
- Warum ist der Wert `NULL` in der `tbl_Hierarchie` nicht zulässig? *(Tipp: Kardinalität)*
- Wann muss eine Hierarchie-Tabelle anstelle einer rekursiven Beziehung eingesetzt werden? *(Tipp: Chefs)*
- Überprüfen Sie ihre SQL-Datentypen-Tabelle: [Check Präsentation](Felddatentypen.pdf)

---

![](../x_res/Buch.jpg)

## Referenzen

