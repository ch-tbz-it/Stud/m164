![TBZ Logo](../x_res/tbz_logo.png)
![m164 Picto](../x_res/m164_picto.jpg)



# m164 - Datenbanken erstellen und Daten einfügen

[TOC]

---

# Tag 8

> ![Teacher](../x_res/Teacher.png) <br> Recap / Q&A Tag 7  <br>
> [Lösung 7.Tag](7T_Loes.md)

# Daten normalisiert einbinden

![ToDo](../x_res/ToDo.png) *Zeit: ca. 3-4 Lektionen, Einzelarbeit*

![Learn](../x_res/Train_D1.png)

Weiterarbeit an der Aufgabe Freifächer von [Tag 7](../7.Tag).

**HINWEIS**: Diese Arbeit ist Vorbereitung auf **LB2**! Arbeiten Sie darum alle Punkte sauber durch! Etwaige Probleme mit Berechtigungen usw. sollten Sie lösen, damit Sie an der LB2 nicht hängen bleiben! 

### Aufgabe DB_Freifaecher nochmals ...

1. Erstellen Sie die **erste Normalform** der EXCEL-Tabelle und exportieren Sie die Daten als CSV-Dateien. (Atomare Felder, Redundanz wird bewusst erzeugt).
2. Erstellen Sie das **log. ERD**. Die (5) Tabellen müssen mind. in der 2.NF sein. Bestimmen Sie die Kardinalitäten.
3. Erstellen Sie das **physische ERD** und setzen Sie die **ersichtlichen, nötigen NN- und UQ-Eigenschaften** (Constraints) bei den Attributen und Fremdschlüsseln. Erzeugen Sie durch z.B. Forward Engineering ein **SQL-DDL-Script** (Datentypen, FK-Constriants, Constaint-Optionen).
4. **Übertragen** Sie die Daten der CSV-Dateien in die normalisierten Tabellen mittels `LOAD DATA LOCAL INFILE` (direkt oder indirekt). Überwachen Sie die korrekte Übertragung der Beziehungen (FK=ID).
5. **Bereinigen** Sie die Daten (Redundanz, leere Werte, Inkonsistenz). Benutzen Sie wenn nötig die Scripte aus [Tag 6!](https://gitlab.com/ch-tbz-it/Stud/m164/-/tree/main/6.Tag?ref_type=heads#finde-redundante-datens%C3%A4tze) als Vorlage.
6. **Testen** Sie die eingelesenen Daten! Vergleichen Sie die Tabellenwerte der CSV-Dateien mit den gleichen, angepassten *Select-Abfragen aus ihrer normalisierten Datenbank*. Die Ausgabelisten sollten dieselben sein!
7. Erzeugen Sie weitere **290 Datensätze** an SchülerInnen (Name, Geb.datum, Klassennummer, Freifachnummer) mit einem Testdatengenerator (evtl. geeignete Datenwerte anpassen, bzw. durchmischen in EXCEL). Führen Sie die obigen Schritte von Punkt 4,5 und 6 erneut durch.
8. Führen Sie folgenden **SELECT-Aufgaben** auf diese Daten aus: <br>
   - Geben Sie aus, wieviele Teilnehmer Inge Sommer in ihrem Freifach hat.
   - Geben Sie eine Liste aller Klassen aus, mit jeweils der Anzahl SchülerInnen (&rarr; FK_SchülerIn) in den Freifächern. Die Liste soll nach den Klassen sortiert werden. 
   - Welche SchülerInnen besuchen das Freifach "Chor" oder "Elektronik"?
9. **Speichern** Sie die Ausgaben von Punkt 8 **in eine Datei**. Benutzen Sie den Befehl `SELECT INTO OUTFILE` dazu. [Siehe Manual](https://mariadb.com/kb/en/select-into-outfile/)
10. Machen Sie ein **Backup** der Datenbank Freifaecher.

![Note](../x_res/note.png) Ablage im Lernportfolio (Scripte und Resultate)

<br><br><br>

##### Freifächer Tipps
- [1.NF (Ansatz)](../7.Tag/media/1NF.png)
- [Log. ERD (2.NF)](../7.Tag/media/logERD_2NF.png)
- [Phys. ERD (2.NF)](../7.Tag/media/physERD_2NF.png)
- [Testdaten Generator Einstellungen](../7.Tag/media/Generator.png) Zufallszahlen z.B. vermischen durch vertikale Verschiebung der Spalten in EXCEL. Korrektur durch "Suchen-Ersetzen".
- [290 Datensätze SchülerInnen](../7.Tag/media/290_SchuelerInnen.csv)

<br><br><br>

---

# Opendata

![ToDo](../x_res/ToDo.png) *Zeit: ca. 2 Lektionen, Einzelarbeit*

![Learn](../x_res/Train_D1.png)

Sie haben bereits im [Modul 162](https://gitlab.com/ch-tbz-it/Stud/m162) das Thema [Opendata](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Daten_Formate/OpenData.md) kennengelernt.

Im Folgenden geht es nun, Ihr Know-How mit Opendata-Quellen anzuwenden. Wählen sie eine Aufgaben aus folgenden Quellen aus:

---

## Steuerdaten Stadt Zürich

Die Stadt Zürich veröffentlicht Steuerdaten von natürlichen Personen. Gehen Sie auf die [Opendata website der Stadt Zürich](https://data.stadt-zuerich.ch/dataset/fd_median_einkommen_quartier_od1003) und laden Sie die aktuellen Steuerdaten (*Median-Einkommen steuerpflichtiger natürlicher Personen nach Jahr, Steuertarif und Stadtquartier*) als CSV-File herunter.

### Aufgaben

1. Analysieren Sie die CSV-Datei und **normalisieren** Sie wo nötig. 
2. Identifizieren Sie die Attribute und legen Sie die Datentypen fest. **Bereinigen** sie die Quelldatei (1.NF, Überflüssige Daten raus).
3. Erstellen Sie ein **phys. ERD** mit dem dazugehörigen DDL-Script. Importieren Sie nun die Steuerdaten in die Datenbank per **Bulkimport**. Schreiben Sie ein DML Skript.
4. **Analysieren** Sie nun die Daten. Was bedeuten die Felder \_p25, \_p50, \_p75? <br>
   Tipp: Es handelt sich um [Grundlagen der Statistik](https://wissenschafts-thurm.de/grundlagen-der-statistik-lagemasse-median-quartile-perzentile-und-modus/).
5.	Lösen Sie folgende **Fragen**: <br>
    a.	Ermitteln Sie das Quartier mit maximalem Steuereinkommen für \_p75?<br>
    b.	Welches Quartier hat das niedrigste Steuereinkommen für \_p50?<br>
    c.	Welches Quartier hat das höchste Steuereinkommen für \_p50?<br>
6. Erstellen Sie ein **Backup** dar Datenbank.

---

## Bildungsdaten Bundesamt für Statistik

Das Bundesamt BFS stellt immer aktualisierte Daten zu nationalen und internationalen Daten zur Verfügung. Gehen Sie zu den [Ausgewählten Bildungsindikatoren im internationalen Vergleich](https://www.bfs.admin.ch/bfs/de/home/statistiken/bildung-wissenschaft.assetdetail.14879800.html) und laden Sie das EXCEL herunter. 

### Aufgaben

1. Analysieren Sie das EXCEL File und **normalisieren** Sie wo nötig. 
2. Identifizieren Sie die **Attribute** und legen Sie die **Datentypen** fest. Bereinigen Sie die Quelldatei (1.NF, Überflüssige Daten raus) und exportieren Sie die Daten in mehrere CSV-Dateien. **Mind. 2018 und 2017 einbeziehen**!
3. Erstellen Sie ein **phys. ERD** mit dem dazugehörigen DDL-Script. Importieren Sie nun die CSV-Daten in die Datenbank per **Bulkimport**. Schreiben Sie ein DML Skript.
4. **Analysieren** Sie nun die Daten. Was sagen Sie aus?   
5.	Lösen Sie folgende **Fragen**: <br>
    a.	Bei welchen Ländern liegt die Schulbesuchsquote der 15-19 Jährigen unter dem OECD-Mittel? Gruppiert pro Jahr!<br>
    b.	Weches Land hat die höchste Anzahl tertiärer Ausbildung über alle (importierten) Jahre hinweg?<br>
    c.	Welches Land hat die grösste Steigerung der Anzahl an abgeschlossenener oblig. Schule innerhalb einers Jahres?<br>
6. Erstellen Sie ein **Backup** dar Datenbank.

---

## Eigene DB aus einer Opendata Quelle

### Aufgaben

1. Suchen Sie eine **geeignete Opendata Quelle** und laden Sie die Daten herunter.
2. **Normalisieren** und erstellen Sie ein **phys. ERD** mit dem dazugehörigen DDL-Script. 
3. Importieren Sie nun die Daten in die Datenbank per **Bulkimport**. Schreiben Sie ein DML Skript.
4. **Analysieren** Sie die Daten und erstellen Sie ein paar Querys dazu
6. Erstellen Sie ein **Backup** dar Datenbank.


![](../x_res/Buch.jpg)

### Opendata Quellen:

[Bundesamt für Statistik](https://www.bfs.admin.ch/bfs/de/home/statistiken.html)

[Daten Stadt Zürch](https://data.stadt-zuerich.ch/dataset)

[Opendata Swiss](https://opendata.swiss/de)

[Open Science Swiss](https://www.uzh.ch/de/researchinnovation/ethics/openscience)

[Open Access Network](https://open-access.network/startseite)






