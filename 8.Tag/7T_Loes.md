![](../x_res/tbz_logo.png)

# M164 Lösungen 7.Tag


![Checkpoint](../x_res/CP.png)



### Nennen Sie den grundsätzlichen Ablauf, wie man Daten in eine normalisierte DB bringt.

1. Rohe Datenliste sichten und in **1.NF** bringen
2. Rohe Daten in die DB in temporäre Tabelle(n) einlesen (`LOAD DATA LOCAL INFILE`).
3. ERD in 3.NF erstellen, Beziehungen und gem. Kardinalitäten erstellen (`Constraints`!) &rarr; normalisierte Tabellen mit DDL erzeugen.
4. Temporäre Tabelle in diese normalisierten Tabellen übertragen (`DML`).
5. Redundanzen elimienieren.
6. Rohe Datenliste mit `SELECT JOIN` nachbilden und Resultat(e) vergleichen und protokollieren (`SELECT IN OUTFILE`).
7. Backup erstellen. (`DUMP`)

###  Was ist der Unterschied zwischen logischem und physischem Backup?

Logische Backups bestehen aus den SQL-Anweisungen, die zur Wiederherstellung der Daten erforderlich sind, wie CREATE DATABASE, CREATE TABLE und INSERT.
Physische Backups werden durch Kopieren der einzelnen Datendateien oder Verzeichnisse durchgeführt.

Die Hauptunterschiede sind wie folgt

- Logische Backups sind flexibler, da die Daten auf anderen Hardwarekonfigurationen, MariaDB-Versionen oder sogar auf einem anderen DBMS wiederhergestellt werden können, während physische Backups nicht auf einer deutlich anderen Hardware, einem anderen DBMS oder möglicherweise sogar einer anderen MariaDB-Version eingespielt werden können. (Migration)
- Logische Backups können auf der Ebene von Datenbanken und Tabellen durchgeführt werden, während physische Datenbanken auf der Ebene von Verzeichnissen und Dateien liegen. In den MyISAM- und InnoDB-Speicher-Engines hat jede Tabelle einen entsprechenden Satz von Dateien. 
- Logische Sicherungen sind größer als die entsprechende physische Sicherung.
- Logische Sicherungen benötigen mehr Zeit für die Sicherung und Wiederherstellung als die entsprechende physische Sicherung.


###  Beschreibe den Restore-Prozess der drei Backup-Typen: FULL / INKREMENTELL / DIFFERENZIELL.

- FULL: Letztes Backup-Set vollständig zurückspielen
- INKREMENTELL: Letztes Full-Backup-Set vollständig und letztes Inkr-BAckup zurückspielen
- DIFFERENZIELL:



###  Nenne drei Möglichkeiten, wie man ein Backup einer Datenbasis machen kann. Wie sind die konkreten Befehle oder das konkrete Vorgehen?

- Logisches Backup: `mysqldump` (Full)
- Physisches Backup: `mariabackup.exe` (Full / Inkr. /Diff.)
- Physisches Backup: `Acronis` Backup SW mit MySQL-Schnittstelle


###  Was sind die (5) Schritte, um eine DB in die 3.NF zu normalisieren?

- 1.NF: Attribute atomar machen (nur 1 Wert/Datentyp pro Attribut)
- 2.NF: <br> a) Gruppieren der Attribute in Entitäten <br> b) Verbinden der Entitäten mit Beziehungen <br> c) Netzwerkbeziehungen mit Zwischentabelle (Transformationstabelle) lösen (1:mc - TT - mc:1)
- 3.NF: Transitive Attribute in eigene Entitäten separieren (&rarr; Stammdaten)

###  Was macht der Befehl `SELECT INTO OUTFILE`?

- Der SQL-Befehl `SELECT INTO OUTFILE` wird verwendet, um die Ergebnisse einer Abfrage in eine Datei zu schreiben. 

	```sql
	SELECT * INTO OUTFILE '/pfad/zur/datei.txt' [Options] FROM tabelle ...;
	```

   Es ist wichtig zu beachten, dass die Datei, in die Sie schreiben möchten, noch nicht existieren darf. Sie kann nicht überschrieben werden. Ausserdem benötigt der Benutzer das `FILE`-Privileg, um diesen Befehl auszuführen. [Siehe Manual](https://mariadb.com/kb/en/select-into-outfile/)

