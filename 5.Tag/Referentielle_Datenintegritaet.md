# Referentielle Datenintegrität


## Aufgabe 1
1.	Weshalb können in professionellen Datenbanken nicht einfach so Daten gelöscht werden?
2.	Wer stellt die referentielle Integrität sicher?

## Aufgaben 2

Verwenden Sie die Lösung von "Tourenplaner" und importieren Sie das Dump-File [tourenplaner_dump](../Daten/tourenplaner_dump.sql)

1.	Als Datenbank-Entwickler stellen Sie fest, dass bei der Dateneingabe ein Fehler passiert ist. Anstatt «4000 Basel» sollte «3000 Bern» heissen. Versuchen Sie in der Tabelle «tbl_ort» die Ortschaft «Basel» zu löschen.
Was stellen sie fest? Erklären Sie!

2.	Was müssten Sie tun, damit nun die richtige Ortschaft in der Datenbank eingetragen sowie die falsch eingegebene Ortschaft gelöscht werden kann?
Setzen Sie Ihren Plan um.


