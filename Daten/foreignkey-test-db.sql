
DROP SCHEMA IF EXISTS `foreignkey_tests` ;
CREATE SCHEMA IF NOT EXISTS `foreignkey_tests` DEFAULT CHARACTER SET latin1 ;

USE `foreignkey_tests` ;


-- -----------------------------------------------------
-- Table `foreignkey-tests`.`kunden`
-- -----------------------------------------------------


-- -----------------------------------------------------
-- Table `foreignkey-tests`.`orte`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `foreignkey_tests`.`orte` ;

CREATE  TABLE IF NOT EXISTS `foreignkey_tests`.`orte` (
  `postleitzahl` VARCHAR(5) NOT NULL ,
  `name` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`postleitzahl`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

LOCK TABLES `orte` WRITE;
INSERT INTO `orte` VALUES ('10000','Musterhausen'),('79092','Freiburg'),('79102','Freiburg'),('79312','Emmendingen');
UNLOCK TABLES;


DROP TABLE IF EXISTS `foreignkey_tests`.`kunden` ;

CREATE  TABLE IF NOT EXISTS `foreignkey_tests`.`kunden` (
  `idkunden` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL DEFAULT NULL ,
  `postleitzahl_FK` VARCHAR(5),  -- ACHTUNG: darf nicht NOT NULL sein für CONSTRAINT SET NULL 
  PRIMARY KEY (`idkunden`) ,
  INDEX `postleitzahl_FK_idx` (`postleitzahl_FK` ASC) ,
  CONSTRAINT `postleitzahl_FK`
    FOREIGN KEY (`postleitzahl_FK` )
    REFERENCES `foreignkey_tests`.`orte` (`postleitzahl` )
    ON DELETE cascade
    ON UPDATE cascade)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = latin1;

--
-- Dumping data for table `kunden`
--

LOCK TABLES `kunden` WRITE;
INSERT INTO `kunden` VALUES (1,'Huber','10000'),(2,'Schmitt','10000'),(3,'Müller','79312'),(4,'Maier','79312'),(5,'Metz','79092'),(6,'Schmied','79102');
UNLOCK TABLES;