-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 02. Apr 2023 um 23:47
-- Server-Version: 10.11.2-MariaDB
-- PHP-Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: Demo `subselect`
--
CREATE SCHEMA IF NOT EXISTS `subselect` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin ;
USE `subselect` ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `country`
--

DROP TABLE IF EXISTS `country` ;
CREATE TABLE `country` (
  `id_country` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `region` varchar(30) NOT NULL,
  `inhabitants` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `country`
--

INSERT INTO `country` (`id_country`, `name`, `region`, `inhabitants`) VALUES
(1, 'Schweiz', 'Europa', 8800000),
(2, 'Österreich', 'Europa', 900000),
(3, 'Deutschland', 'Europa', 83300000),
(4, 'Spanien', 'Europa', 47400000),
(5, 'Italien', 'Europa', 58900000),
(6, 'Russland', 'Europa', 144000000),
(7, 'Königreich', 'Europa', 6750000),
(8, 'Ukraine', 'Europa', 41000000),
(9, 'USA', 'Amerika', 333287557),
(10, 'Kanada', 'Amerika', 37000000);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--
DROP TABLE IF EXISTS `users` ;
CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `age` int(11) NOT NULL,
  `country` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id_user`, `name`, `age`, `country`) VALUES
(1, 'King', 23, 'USA'),
(2, 'Küng', 33, 'Schweiz'),
(3, 'Trumpel', 99, 'Jail'),
(4, 'Meier', 45, 'Deutschland'),
(5, 'Knie', 45, 'Österreich'),
(6, 'Kummer', 45, 'Schweiz'),
(7, 'Querra', 45, 'Spanien'),
(8, 'Forrester', 29, 'Kanada');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id_country`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `country`
--
ALTER TABLE `country`
  MODIFY `id_country` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
