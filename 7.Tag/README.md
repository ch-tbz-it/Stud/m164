![TBZ Logo](../x_res/tbz_logo.png)
![m164 Picto](../x_res/m164_picto.jpg)



# m164 - Datenbanken erstellen und Daten einfügen

[TOC]

---

# Tag 7

> ![Teacher](../x_res/Teacher.png) <br> Recap / Q&A Tag 6  <br>
> [Lösung 6.Tag](6T_Loes.md)

# DB Backup

![Learn](../x_res/Learn.png)

> ![Teacher](../x_res/Teacher.png) <br> Backup Konzepte

![Overview](./media/Backup.jpg)

# Datensicherung von Datenbanken

Datenbanksysteme spielen sowohl beim Webhosting wie auch als Bestandteil von Unternehmenssoftware eine große Rolle. Denn von der Verfügbarkeit und Vollständigkeit der gespeicherten Daten hängen die Funktionalität der Website bzw. die Aktionsfähigkeit des Unternehmens ab.

Website-Projekte, die auf Datenbanken zugreifen, entnehmen selbigen z.B. mithilfe verschiedener Skript-Sprachen alle Informationen, die für das korrekte Anzeigen der Seite notwendig sind. Auch die IT-Infrastruktur eines Unternehmens zieht ihre Informationen in der Regel aus den zugrundeliegenden Datenbanken. Gleichzeitig findet der **Datenaustausch** ebenso in umgekehrter Richtung statt, indem Benutzer Daten in der Datenbank ablegen bzw. speichern. So beinhalten Datenbanksysteme nicht selten Personal- und Finanzinformationen oder sensible Kundendaten. Entsprechend schwer wiegt ein Ausfall der Datenbank oder gar ein Datenverlust: Die Website präsentiert die Inhalte nicht mehr vollständig oder ist komplett offline, Anwendungen funktionieren nicht mehr und die Kundendaten sind lückenhaft oder fehlen schlimmstenfalls komplett. Das sorgt für zusätzliche Arbeit beim Betroffenen und Irritation beim Kunden, die möglicherweise einen dauerhaften Vertrauensverlust zur Folge hat.

Die Ursache für einen Datenverlust ist in den meisten Fällen kein Angriff von außen, sondern technisches Versagen der Hardware oder schlicht ein Benutzerfehler. Davor kann auch die beste Sicherheitssoftware nicht schützen. Damit der Datenverlust nicht irreversibel ist, ist eine Datensicherung erforderlich.

## Möglichkeiten zur Sicherung von Datenbanken

Um einem Datenverlust vorzubeugen, sollte man Sicherheitskopien der Datenbanken auf externen Speichermedien erstellen. Mit diesen Kopien, die auch Backups genannt werden, kann der Zustand der Datenbank zum Zeitpunkt der Datensicherung wiederhergestellt werden.

Hierbei ist zunächst zwischen Online- und Offline-Backups zu unterscheiden: Online-Backups werden erzeugt, ohne dass die Datenbank heruntergefahren werden muss. Während des Sicherungsprozesses nimmt die Datenbank vorgenommene Änderungen in einen separaten Bereich auf und fügt sie erst im Anschluss an die Sicherung in die entsprechende Datei ein. Fährt man die Datenbank für die Zeit der Sicherung herunter, spricht man von einem Offline-Backup. Dieses Verfahren zur Datensicherung hat zwar den Vorteil, dass es relativ unkompliziert durchzuführen ist, bringt jedoch auch den Nachteil mit sich, dass die Anwendungen oder die Websites während des Backups nicht verfügbar sind. Daher sollte ein Offline-Backup möglichst in der Nacht bzw. zu Zeiten geringen Datenverkehrs durchgeführt werden.

Zusätzlich zur Unterteilung in Online- und Offline-Backup unterscheidet man beim Überspielen der Daten auf ein Sicherheitsmedium folgende drei Arten der Sicherung:

-   **Voll-Backup**: Wie der Name schon verrät, werden bei dieser Art der Datensicherung immer sämtliche Daten (und Strukturen) überspielt. Das hat zur Folge, dass pro Backup zwar alle Dateien vorliegen, der benötigte Speicherplatz bei häufiger Sicherung jedoch sehr hoch ist. Für die Wiederherstellung wird nur das jeweilige Voll-Backup benötigt.
-   **Differentielles Backup**: Bei einem differentiellen Backup wird zunächst ein Voll-Backup erstellt. Die folgenden Sicherungen unterscheiden sich von der ersten insofern, dass lediglich Dateien gesichert werden, die sich geändert haben oder neu hinzugekommen sind. Im Gegensatz zum Voll-Backup wird so **Speicherplatz** gespart. Allerdings werden geänderte und neue Dateien – bis zum nächsten Voll-Backup – bei jedem differentiellen Backup erneut kopiert. Die Recovery gelingt nur mit dem letzten Voll-Backup inklusive gewünschtem differentiellen Backup.
-   **Inkrementelles Backup**: Im Anschluss an ein komplettes Backup werden bei der inkrementellen Datensicherung nur die Dateien kopiert, die sich seit der letzten Sicherung geändert haben oder neu hinzugekommen sind. Im Unterschied zu der differentiellen Methode bezieht sich ein inkrementelles Backup immer auf das vorherige (sowohl Voll-Backup als auch inkrementelles Backup). So wird jede Datei nur einmal gesichert und dadurch der Speicherplatz geschont. Um die gewünschten Dateien wiederherzustellen, werden allerdings alle Sicherungen vom Voll-Backup bis zum gewünschten Stand benötigt.

![backuptypes](./media/backuptypes.webp)

Es gibt also einige Optionen, Datenbanksysteme wie SQL-Datenbanken oder [Microsoft Access](https://www.ionos.de/digitalguide/server/tools/microsoft-access-alternativen-im-ueberblick/) zu sichern. Welche Sicherungsmethode jeweils am besten geeignet ist, hängt vom Anforderungsprofil des Users bzw. Unternehmens ab. Selten durchgeführte Sicherungen zum Sparen von Speicherplatz sollten aber niemals einen Lösungsweg darstellen. Externe Speichermedien wie z. B. Festplatten sollten zudem sicher und in einem separaten Bereich aufbewahrt werden, wo sie gegen Diebstahl, aber auch Brandschäden geschützt sind. Zusätzlich sollten die gesicherten Daten auch verschlüsselt werden, damit sie im Falle eines Diebstahls nicht von Dritten genutzt werden können.

## Backups erzeugen

Hat man sich für eine Backup-Lösung entschieden, besteht der nächste Schritt darin, sich für eine Durchführungsmethode zu entscheiden. Es gibt verschiedene Möglichkeiten und Tools, um die Sicherung von Datenbanken, beispielsweise einer SQL-Datenbank, in die Wege zu leiten. Die folgende Auflistung erläutert einige davon:

-   **MySQLDump**: Wer über einen Shell-Zugang verfügt, kann mit der integrierten (Voll-)Backup-Funktion von MySQL und dem Befehl „mysqldump“ arbeiten. Allerdings erlauben nicht alle Hoster den Zugriff auf diese Funktion, die im Übrigen die schnellste Backup-Durchführung ermöglicht.
-   **phpMyAdmin**: Mit dieser Administrations-Plattform für SQL-Datenbanken können Benutzer die gewünschte Datenbank einfach im gewünschten Format, z. B. SQL, exportieren. Allerdings kann es dazu kommen, dass das **PHP-Script** bei zu großen Datenbanken vom Server abgebrochen wird. Zudem funktioniert nur die Einspielung von Backups mit einer Grösse von bis zu 2 MB.
-   **BigDump**: Das Tool BigDump bietet die perfekte Ergänzung zu phpMyAdmin, da es beliebig große Backups wieder einspielen kann. Eine eigene Sicherungs-Funktion bietet es allerdings nicht.
-   **HeidiSQL**: Die Backup-Lösung für Windows-Systeme basiert nicht auf PHP und hat daher keine Probleme mit großen Backups. Dem Tool, das ansonsten phpMyAdmin sehr ähnlich ist, fehlt allerdings die Möglichkeit zur Automatisierung des Sicherungsprozesses.
-   **Mariabackup**: Ist ein Open-Source-Tool, das von MariaDB bereitgestellt wird, um physische Online-Backups von InnoDB, Aria und MyISAM-Tabellen durchzuführen. Es wurde ursprünglich von Percona XtraBackup abgezweigt. Es ist auf Linux und Windows verfügbar.

## Datenbanken müssen immer geschützt werden

Die in den Datenbanken gespeicherten Files haben häufig eine hohe Bedeutung für den reibungslosen Ablauf in Betrieben oder die korrekte Anzeige von Websites. Webserver greifen auf die enthaltenen Informationen zu, um die gehostete Website fachgerecht darstellen zu können, und auch die Funktionalität von Anwendungen im Netzwerk ist oftmals direkt mit einer Datenbank verbunden. Datenbanken bilden zudem auch den Speicherort sensibler Daten wie Adressen, Kontoinformationen oder Telefonnummern.

Aufgrund ihrer wichtigen Rolle sollte man die **Datenbanksysteme** unbedingt mit entsprechenden Sicherheitsmaßnahmen schützen. Während die Daten auf der einen Seite vor Angriffen von außen geschützt werden müssen, droht auf der anderen Seite nämlich auch der Verlust durch interne Probleme wie z. B. Hardware-Defekte oder Benutzerfehler. Regelmäßige Backups beugen einem Datenverlust vor und garantieren dadurch eine langfristige Datensicherheit.

<br>

**Hinweis**: 
Es macht Sinn, dass ein eigener Backup-User für logische Backups eingerichtet wird:

```SQL
GRANT RELOAD, PROCESS, LOCK TABLES, REPLICATION CLIENT ON *.* TO 'backupuser'@'localhost' IDENTIFIED BY 'backup123';
```

<br><br><br>

### Auftrag 

![ToDo](../x_res/ToDo.png) *Zeit: ca. 45 Min*

![Learn](../x_res/Train_R1.png)

1. Fassen Sie den obigen Text in ihrem Lernportfolio zusammen.
2. Lösen Sie folgende Aufträge: [Datensicherung](./Datensicherung.md)

![Note](../x_res/note.png) Ablage im Lernportfolio (Scripte und Resultate)

---

# Daten normalisiert einbinden (DB Freifächer)

![ToDo](../x_res/ToDo.png) *Zeit: ca. 4-6 Lektionen, Einzelarbeit*

![Learn](../x_res/Train_D1.png)

**HINWEIS**: Diese Arbeit ist Vorbereitung auf **LB2**! Arbeiten Sie darum alle Punkte sauber durch! Etwaige Probleme mit Berechtigungen usw. sollten Sie lösen, damit Sie an der LB2 nicht hängen bleiben! 

---

Im Folgenden soll gezeigt werden, wie man eine Datentabelle in eine normalisierte Datenbank einlesen kann, um sie dann als Quelle für Abfragen nutzen zu können!


### Story Freifächer
Innerhalb einer Schul-DB soll ein DB-Teil so eingerichtet werden, dass zusätzlich zum regulären Unterricht Freifächer verwaltet werden können.
Folgende Bedingungen sind zu erfüllen (nur die hierfür notwendigen Tabellen erstellen):

- Ein Schüler hat eine Nr und eine Stammklasse
- Die SchülerInnen können sich in beliebige Freifächer eintragen
- Klassenlehrer sind auch Lehrer von Lernangeboten
- Ein Freifach hat eine Beschreibung (Nr/Thema), ein Zimmer und wird an einem bestimmten Tag angeboten
- Eine Freifach wird von einem Lehrer unterrichtet

Aktuelle Daten: 

[Freifachliste EXCEL](./Freifaecher.xls)

### Aufgabe

1. Erstellen Sie die **erste Normalform** der EXCEL-Tabelle und exportieren Sie die Daten als CSV-Dateien. (Atomare Felder, Redundanz wird bewusst erzeugt).
2. Erstellen Sie das **log. ERD**. Die (5) Tabellen müssen mind. in der 2.NF sein. Bestimmen Sie die Kardinalitäten. 
3. Erstellen Sie das **physische ERD** und setzen Sie die **ersichtlichen, nötigen NN- und UQ-Eigenschaften** (Constraints) bei den Attributen und Fremdschlüsseln. Erzeugen Sie durch z.B. Forward Engineering ein **SQL-DDL-Script** (Datentypen, FK-Constriants, Constaint-Optionen).
4. **Übertragen** Sie die Daten der CSV-Dateien in die normalisierten Tabellen mittels `LOAD DATA LOCAL INFILE` (direkt oder indirekt). Überwachen Sie die korrekte Übertragung der Beziehungen (FK=ID).
5. **Bereinigen** Sie die Daten (Redundanz, leere Werte, Inkonsistenz). Benutzen Sie wenn nötig die Scripte aus [Tag 6!](https://gitlab.com/ch-tbz-it/Stud/m164/-/tree/main/6.Tag?ref_type=heads#finde-redundante-datens%C3%A4tze) als Vorlage.
6. **Testen** Sie die eingelesenen Daten! Vergleichen Sie die Tabellenwerte der CSV-Dateien mit den gleichen, angepassten *Select-Abfragen aus ihrer normalisierten Datenbank*. Die Ausgabelisten sollten dieselben sein!
7. Erzeugen Sie weitere **290 Datensätze** an SchülerInnen (Name, Geb.datum, Klassennummer, Freifachnummer) mit einem Testdatengenerator (evtl. geeignete Datenwerte anpassen, bzw. durchmischen in EXCEL). Führen Sie die obigen Schritte von Punkt 4,5 und 6 erneut durch.
8. Führen Sie folgenden **SELECT-Aufgaben** auf diese Daten aus: <br>
   - Geben Sie aus, wieviele Teilnehmer Inge Sommer in ihrem Freifach hat.
   - Geben Sie eine Liste aller Klassen aus, mit jeweils der Anzahl SchülerInnen (&rarr; FK_SchülerIn) in den Freifächern. Die Liste soll nach den Klassen sortiert werden. 
   - Welche SchülerInnen besuchen das Freifach "Chor" oder "Elektronik"?
9. **Speichern** Sie die Ausgaben von Punkt 8 **in eine Datei**. Benutzen Sie den Befehl `SELECT INTO OUTFILE` dazu. Ein Error 13 "Permission denied" könnte mit fehlenden Berechtigungen auf One-Drive-Ordner zusammenhängen. Wahlen Sie z.B. den Export-Ordner `C:/M164/outfile.csv`. [Siehe Manual](https://mariadb.com/kb/en/select-into-outfile/)
10. Machen Sie ein **Backup** der Datenbank Freifaecher.

![Note](../x_res/note.png) Ablage im Lernportfolio (Scripte und Resultate)

<br><br><br>

---



![Checkpoint](../x_res/CP.png)

#### Checkpoint

- Nennen Sie den grundsätzlichen **Ablauf**, wie man Daten in eine normalisierte DB bringt.
- Was ist der Unterschied zwischen **logischem und physischem Backup**?
- Beschreibe den **Restore-Prozess** der drei Backup-Typen: FULL / INKREMENTELL / DIFFERENZIELL.
- Nenne **drei Möglichkeiten**, wie man ein **Backup einer Datenbasis** machen kann. Wie sind die konkreten Befehle oder das konkrete Vorgehen?
- Was sind die (5) Schritte, um eine **DB in die 3.NF zu normalisieren**?
- Was macht der **Befehl** `SELECT INTO OUTFILE`?

<br><br><br>

---

![](../x_res/Buch.jpg)

## Referenzen

- [Datensicherung](https://www.ionos.de/digitalguide/server/sicherheit/datensicherung-von-datenbanken/)
- [MariaDB Backup Tool](https://mariadb.com/kb/en/mariabackup-overview/)

---

##### Freifächer Tipps
- [1.NF (Ansatz)](./media/1NF.png)
- [Log. ERD (2.NF)](./media/logERD_2NF.png)
- [Phys. ERD (2.NF)](./media/physERD_2NF.png)
- [Testdaten Generator Einstellungen](./media/Generator.png) Zufallszahlen z.B. vermischen durch vertikale Verschiebung der Spalten in EXCEL. Korrektur durch "Suchen-Ersetzen".
- [290 Datensätze SchülerInnen](./media/290_SchuelerInnen.csv)

