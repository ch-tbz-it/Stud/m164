![](../x_res/tbz_logo.png)

# M164 Lösungen 6.Tag


![Checkpoint](../x_res/CP.png)



### Formulieren Sie einen Satz, der den **Einsatz** von Subqueries erklärt und begründet.

- In SQL sind Unterabfragen (Subqueries) leistungsstarke Werkzeuge, die es ermöglichen, komplexe Abfragen zu erstellen, indem sie eine Abfrage innerhalb einer anderen Abfrage ausführen. Sie können verwendet werden, um Daten zu filtern, zu sortieren oder zu aggregieren, die auf den Ergebnissen einer anderen Abfrage basieren. Dies ist besonders nützlich, wenn die Daten, die Sie abrufen möchten, auf mehreren Tabellen verteilt sind oder wenn Sie eine Berechnung auf einer Gruppe von Daten durchführen möchten, bevor Sie diese mit anderen Daten vergleichen. 

### Was ist der Unterschied zwischen skalaren und nicht-skalaren Subqueries?

- **Skalare Unterabfragen**: Diese Art von Unterabfragen gibt genau einen Wert (eine einzelne Zelle) zurück. Sie können an Stellen verwendet werden, an denen ein einzelner Wert erwartet wird, wie z.B. in einer WHERE-Klausel oder als Teil einer Berechnung in einer SELECT-Anweisung. Ein Beispiel für eine skalare Unterabfrage könnte sein, den Durchschnittspreis aller Produkte zu ermitteln.

- **Nicht-skalare Unterabfragen**: Diese Art von Unterabfragen kann mehrere Zeilen und/oder mehrere Spalten zurückgeben. Sie werden oft in Kombination mit Operatoren wie IN, EXISTS, ALL und ANY verwendet. Ein Beispiel für eine nicht-skalare Unterabfrage könnte sein, eine Liste von Produkt-IDs zu ermitteln, die in einer anderen Tabelle referenziert werden.

Der Hauptunterschied zwischen den beiden liegt also in der Art der zurückgegebenen Daten und in der Art und Weise, wie sie in der übergeordneten Abfrage verwendet werden können. Skalare Unterabfragen sind in der Regel einfacher zu verwenden und zu verstehen, während nicht-skalare Unterabfragen mehr Flexibilität bieten, aber auch komplexer sein können.

### Gibt es auch Gefahren bei der Verwendung von Subselect?

- Ja. Da zwischen den Tabellen des übergeordneten Befehls keine definierte Beziehung zu der Tabelle des Subquerys besteht, kann es sein, dass die Resultate nicht der Realität entsprechen! Siehe Beispie `customers` - `orders`.

### Was bedeutet der Zusatz `IGNORE 1 LINES` in `LOAD DATA INFILE`? 

- Die Klausel IGNORE 1 LINES in LOAD DATA INFILE wird verwendet, um Zeilen am Anfang der Datei zu ignorieren. Dies ist besonders nützlich, wenn die erste Zeile der Datei Spaltennamen oder andere Kopfdaten enthält, die nicht in die Tabelle eingefügt werden sollen.


### Sie haben eine Windows-CSV-Datei. Der Import ist aber mit `LINES TERMINATED BY '/n';` eingestellt. Welche Folgen hat das für die letzte Spalte (Attribut) der importierten Daten? Gibt es ein Problem?

- Die Windows-Datei markiert das Zeilenende mit "\r\n". ( = CR LF ) Beim Importieren wird "\r” ignoriert, bzw. in die Datenspalte übertragen. Z.B. aus einen Text "CH" in der letzten Spalte wird ein Text "CH\r". Beim vertikalen Selektieren der Datensätze mit `WHERE Country = "CH"` wird der Datensatz nicht gefunden! Selektiert wird er erst mit `WHERE Country like "CH%"`!

### Welche Einstellungen müssen gemacht werden, damit ein Klient eine CSV-Datei dem Server zum Importieren übermitteln darf?

- **Klient**: <br> `SET opt_local_infile = 1;`
- **Server**: <br> `SET GLOBAL local_infile=1;` <br> `SET secure_file_priv = "";`

### Wie importieren Sie Spalten in einer anderen Reihenfolge (z.B. 3.NF Tabellen)? 

- **1. Möglichkeit**: Alle Spalten der CSV-Datei in eine aquivalente Tabelle in der Datenbasis einlesen: Mit `INSERT .. SELECT ..` einzelne Attribute auf andere Tabellen verteilen.
- **2. Möglichkeit**: Mit `LOAD DATA INFILE mit COL-Angaben` die richtigen CSV-Spalten jeweils auf die Tabellen-Attribute verteilen: <br> ![LDIF Add Attribute](./media/LDIF_Col.png)
- **Erklärung**: Wenn Sie eine Spalte überspringen möchten, führen Sie einfach eine Dummy-Variable `@var` an der Stelle ein. Die Variable "schluckt" dann diese Spalte: <br> ![LDIF Skip Attributes](../6.Tag/media/LDIF_Skip.png) <br> Ebenso kann eine zusätzliches Attribut in der DB-Tabelle gesetzt werden: ![LDIF Skip Attributes](../6.Tag/media/LDIF_Add.png)