# Auftrag Datensicherung

Zeit: 50 Min.<br>
Form: 2er Team

In diesem Auftrag erzeugen wir ein logisches Datenbank-Backup mit dem Tool **mysqldump**, u.a. soll Ihre Datenbank **Tourenplaner** gesichert werden.
Mit MariaDB lässen sich auch physische Backups im Betrieb erstellen. Führen Sie die folgende Aufgaben gemäss ihrer Installation aus. Überspringen Sie jene, die Sie nicht installiert haben.

---
## Aufgabe 1: Logisches Backup erstellen

### Backup erstellen mit MySQLDump.exe (XAMPP, MariaDB, MySQL)

Suchen Sie den Ordner `C:\....\MYSQL\BIN` in ihrem System. Finden Sie darin das Programm `MySQLDump.exe`. 

Das Hilfsprogramm erzeugt ein **Script mit DDL und DML** Befehlen, welches beim **Restore** einfach wieder eingelesen werden kann. `MySQLDump.exe` gibt den Dump grundsätzlich auf den Bildschirm aus. Mittels der angehängten Umleitung `... > c:\Pfad\backup.sql `wird die Ausgabe dann in eine Datei geschrieben. MySQLDump muss *Schreibrechte* auf diesem Ausgabe-Pfad haben! 

> Beispiel falls Port vom SQL-Server umgestellt wurde:
> 
```cmd
    C:\....\MYSQL\BIN\mysqldump -u root -p --port=3306 tourenplaner > C:\BACKUP\tp_dump.sql
```

Falls Ihr Datenbank mit XAMPP läuft, folgen Sie der Anleitung unter [Generate the backup of a single database](https://www.sqlshack.com/how-to-backup-and-restore-mysql-databases-using-the-mysqldump-command/).

<br><br>

### Variante WORKBENCH: Logisches Backup erstellen mit MS Workbench

> ![Wichtig](../x_res/Wichtig.png) Achtung: Workbench startet per Default das eigene im eigenen Verzeichnis gespeicherte `MySQLDump.exe`, welches nicht kompatibel mit MariaDB ist! Falls Sie MariaDB **10.11.x** installiert haben, können Sie folgende Einstellung anpassen. <br> <br> ![Einstellung](./media/WB_EXPORT_Setting.png) <br> <br> ![Wichtig](../x_res/Wichtig.png) Achtung: Mit MariaDB **11.x.x** ist **kein Dump in Workbench** möglich!

**Backup einer oder mehrerer Datenbanken:**

![Export](./media/WB_EXPORT.png)

<br><br>

### Variante XAMPP: Logisches Backup erstellen mit phpMyAdmin
Unter Exportieren haben Sie die Möglichkeit alle oder einzelne DBs des Servers zu sichern.

**Backup des Servers:**

![Backup Server](./media/XAMPP-pma-Server.png)

**Backup einer Datenbank:**

![Backup Server](./media/XAMPP-pma-Db.png)

Das Dump-File landet im Download-Ordner!

<br><br>

### Variante DOCKER: Logisches Backup erstellen mit DOCKER

In der Container-Technologie werden u.a. Befehle im Container ausgeführt. Ergebnisse (z.B. Files) müssen danach vom Container zum Host (Computer) kopiert werden.

Nachfolgend eine schrittweise Anleitung, wie Sie im Container ein file mit dem Tool «mysqldump» generieren und danach zum Host kopieren.

1.	Legen Sie einen neuen Ordner in ihrem Computer an, z.B. «C:\db_backup».
2.	Öffnen Sie nun ein das Commandline-Tool «cmd».
3.	Führen Sie diese Zeile aus:<br>
    ```bash
    docker exec -it mysql /usr/bin/mysqldump -u root -p --databases tourenplaner --result-file=/var/lib/mysql/tourenplaner_backup.sql
    ```

    Eine Eingabeaufforderung erscheint. Geben Sie das Passwort ein (Gleich wie beim MySql Connection)

    ![](../../x_res/cmd_eingabeaufforderung.png)

    Sobald Sie das Passwort eingegeben haben, wird hier im container das Tool «mysqldump» ausgeführt. Dieses erstellt ein Dump (DDL und DML Befehle der Datenbank) und schreibt diesen ins file «tourenplaner_backup.sql» rein.

4.	Führen Sie als Nächstes diese Zeile aus: <br>
    ```bash
    docker cp mysql:/var/lib/mysql/tourenplaner_backup.sql c:\db_backup\
    ```
    Das file «tourenplaner_backup.sql» wird vom Container zum Host (Computer) kopiert. 

---

## Aufgabe 2: Backup-File analysieren und verifizieren

1.	Ihr Dumpfile (Backup-File) ist nun nicht mehr leer. Analysieren Sie den Inhalt.
    - Was finden Sie vor?
    - Beschreiben Sie ihn.
2.	Überprüfen Sie ihr Dump-File (Restore)
    - Indem Sie die Datenbank `tourenplaner` löschen. 
    - Führen Sie nun das Dump-File aus. 
    - Ihre Datenbank sollte wiederhergestellt sein!

    > **Hinweis**: Mit dem Befehl **SOURCE** können Sie das Dump-File direkt im CL-Klienten einlesen:
    >
    ```sql
    mysql> source C:\MySQLBackup\dump.sql
    ```
    ***Evtl. muss die Datenbasis zuerst erzeugt werden, bevor Sie einlesen!!***


## Aufgabe 3: Backup Strategien

1.	Öffnen Sie diese [Seite](https://dev.mysql.com/doc/refman/8.0/en/backup-types.html) und lesen Sie das erste Kapitel **«Physical (Raw) Versus Logical Backups»**<br>
    a.	Welchen Backup haben wir in **Aufgabe 1**  erstellt?<br>
    b.	Welche Nachteile hat dieses Backup?<br>
2.	Was ist der Unterschied zwischen online- und offline Backups?
3.	Was ist ein «snapshot Backup»?


## Aufgabe 4: Physisches Backup (Variante MariaDB, XAMPP)

- Erstellen Sie ein Full-Backup mit dem Backup-Programm `mariabackup.exe`. 
- Wozu muss die Backupdatenstruktur vor einem Restore "vorbereitet" (`prepare`) werden?
- Wie heist der Parameter, um ein `Restore` zu machen?
- Ist ein inkrementelles und differentielles Backup möglich? Wenn ja, wie?

[Mariabackup Manual](https://mariadb.com/kb/en/mariabackup-overview/#using-mariabackup)

## Aufgabe 5: Backup mit externem Backup-Programm

Studieren Sie folgende Anleitung für Backup und Restore: [Backup mit Acronis](https://www.acronis.com/de-de/blog/posts/mysql-backup/)

<br><br><br>

---

![](../x_res/Buch.jpg)

# Referenz zu den Unterlagen, Aufträgen und Vorgaben

1. [MySQL Dump](https://dev.mysql.com/doc/refman/8.0/en/mysqldump.html)
