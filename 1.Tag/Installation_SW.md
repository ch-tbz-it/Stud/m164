![TBZ Logo](../x_res/tbz_logo.png)

[TOC]

# m164 - Installation, Manuals & Tutorials

## 1) MySQL-Workbench (M162 - WIN macOS)

Für das Absetzen der SQL-Statements verwenden wir vorzugsweise das im M162 installierte
**MySQL-Workbench**: [https://dev.mysql.com/downloads/workbench/](https://dev.mysql.com/downloads/workbench/)

### Workbench Manual & Tutorials:

[MySQL Workbench Manual](https://dev.mysql.com/doc/workbench/en/)

[Youtube MySQL Workbench Tutorial](https://www.youtube.com/watch?v=X_umYKqKaF0)

[Youtube MySQL innodb workbench Referentielle Integrität](https://www.youtube.com/watch?v=Y6ymh8MSkxQ)

---

## 2) MySQL Server installieren

Sie haben verschiedene Möglichkeiten einen MySQL Server, oder den Fork MariaDB, zu installieren. Es kann auch sein, dass
eine Lehrperson eine
Variante vorgibt.

| Art                | Runtime |                Weiteres                 | Anleitung                                                           |
|--------------------|:-------:|:---------------------------------------:|---------------------------------------------------------------------|
| Standalone MySQL   |  Lokal  |                    -                    | [MySQL Community Server](https://dev.mysql.com/downloads/mysql)     |
| Standalone MariaDB |  Lokal  |                    -                    | [MariaDB Server](https://mariadb.org/download/?t=mariadb&p=mariadb) |
| XAMPP              |  Lokal  |           Apache, phpMyAdmin            | [XAMPP Anleitung](./XAMPP/README.md)                                |
| Docker             |  Lokal  |                    -                    | [Docker Anleitung](./Docker/Readme.md)                              |
| AWS Cloud          | Remote  | Muss von Lehrperson freigeschaltet sein | [AWS Anleitung](./AWSCloud/Readme.md)                               |

---

## 3) Den Datenbank-Server mit mySQL Workbench verbinden

Ihre Datenbank ist nun lokal installiert und betriebsbereit.

Öffnen Sie nun mySQL workbench und gehen Sie auf die Startseite.

![](../x_res/workbench_start.png)

Erstellen Sie nun eine neue mySQL Verbindung, indem Sie auf «PLUS» klicken.

![](../x_res/workbench_connection.png)

Ein neues Fenster erschein. Geben Sie nun Ihren eigenen Connection Name ein und ändern Sie den Port auf 3306 (
Standardport) für XAMPP oder AWS Installation und 3307 für die Docker Installation.

![](../x_res/workbench_connection_port.png)

Machen Sie nun eine «Test Connection» und geben Sie das root Passwort «password» ein.

![](../x_res/workbench_password.png)

Nachdem Sie Ihre Eingabe bestätigt haben, erscheint nun diese Meldung. Sie haben sich nun erfolgreich mit ihrer lokalen
Datenbank verbunden!

![](../x_res/workbench_connection_successful.png)

Ihre Datenbank steht nun zur Verfügung. Nun sind Sie bereit, ihr eigenes **Modell** in der Datenbank, bzw. **SQL-Befehle
** im Editor zu erstellen.

![](../x_res/workbench_db_ready1.png)<br>
![](../x_res/workbench_db_ready2.png)







				