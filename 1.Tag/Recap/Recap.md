![TBZ Logo](../../../x_res/tbz_logo.png)
![m319 Picto](../../../x_res/m164_picto.jpg)

[TOC]

# ---- Recap m162 (KEL) ---

**Zeitvorgabe**: 15 Minuten <br>
**Form**: Individuell

--

## Fragen:

1. Welche Stufen gibt es bei der **Wissenstreppe**? <br>
   Nennen Sie diese der Reihe nach und machen Sie ein Beispiel mit einem Wechselkurs.
2. Wie werden **Netzwerk-Beziehungen** im **logischen** Modell abgebildet? 
3. Was sind **Anomalien** in einer Datenbasis? Welche Arten gibt es?
4. Gibt es redundante "**Daten**"? Warum?
5. **Datenstrukturierung bei der Erhebung und Ablage von Daten**: <br> Welche zwei Aspekte können strukturiert werden? <br> Welche Kategorien (Abstufungen) gibt es bei der Strukturierung? <br> Und wie müssen die Daten in einer DB strukturiert sein?
6.    Beschreiben das Bild mit den richtigen Fachbegriffen <br>
   ![Terminologie](./Tabelle_labelled.png)
7. Welche (einschränkenden) **Einstellungen** zu den Attributen (z.B. ID) kennen Sie? <br> ![Constriant](./WB_Constraints.png)