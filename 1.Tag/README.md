![TBZ Logo](../x_res/tbz_logo.png)√
![m164 Picto](../x_res/m164_picto.jpg)


# m164 - Datenbanken erstellen und Daten einfügen

[TOC]


---

# Tag 1: Einführung, Recap M162 

![Teacher](../x_res/Teacher.png)

- Vorstellung Klasse und LP: Regeln, etc.
- Einführung Modul (ID, Struktur Teams, GitLAB, [Lernportfolio](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-Portfolio#wahl-eines-tools))
	
	**Modulbeschreibung:**
	
	Implementiert ein logisches, relationales Datenmodell in einem Datenbankmanagementsystem. Fügt Daten in die Datenbank ein, prüft die eingefügten Daten und korrigiert allfällige Fehler. <br> &#8594; Einige Voraussetzungen sind im Modul 162 zu finden (Normalisierung, Design).
	
	[ICT-Berufsbildung](https://www.modulbaukasten.ch/module/164/1/de-DE?title=Datenbanken-erstellen-und-Daten-einf%C3%BCgen)   
	[Modul-ID als PDF](../Unterlagen/164_1_Datenbanken_erstellen_und_Daten_einfuegen.pdf)
	
	**Mögliche LBVs:**
	
	[LBV 164-1](https://gitlab.com/ict-modulformation-ch/module/m164/m164-lbv-1/-/blob/master/LBV_M164-1.md) 2-3 Kompetenznachweise (⅓ Theorie, ⅔ Praxis)
	
	[LBV 164-3](https://gitlab.com/ict-modulformation-ch/module/m164/lbv-modul-164-3) 1 Prüfung & 1 Projekt für Fortgeschrittene
	

- **Modulübersicht**: <br> 
	![moduluebersicht_m164](../x_res/m164_modulübersicht-Theorie.drawio.png)

---

![Reflect](../x_res/Train_R1.png)

# Recap Fachbegriffe und Konzepte (Modul 162)



   - Auftrag [Recap](./Recap/Recap.md)



# Recap Normalisieren (Modul 162)

## Grundlagen Datenmodellierung

„In der Informatik, im Besonderen bei der Entwicklung von Informationssystemen, dienen **Datenmodelle** und die zu deren Erstellung durchgeführten Aktivitäten (**Datenmodellierung**) dazu, die Struktur für die in den Systemen zu verarbeitenden (im Besonderen für die zu speichernden) Daten zu finden und festzulegen.“ (Wikipedia &rarr; Datenmodell)

Es geht darum, Informationsstrukturen zu modellieren, um dann darauf basierend die Implementierung von Datenbanken umzusetzen. Datenmodellierung ist also eine Phase innerhalb der Datenbankentwicklung.

In der Phase Datenmodellierung werden dabei drei Datenmodelle entwickelt, die aufeinander aufbauen:

1.  Konzeptionelles Datenmodell (ERM)
2.  Logisches Datenmodell (ERD)
3.  Physisches Datenmodell

Das **konzeptionelle Datenmodell** (Entity Relationship Model) ist dabei nicht-implementierungsabhängig und bildet somit reine Fachlichkeit ab. Es wird direkt aus den Anforderungen heraus entwickelt. 

Das **logische Datenmodell** (Entity Relationship Diagram) ist die Abbildung des konzeptionellen Datenmodells auf das zu verwendende Datenbanksystem, d.h. ein relationales Datenmodell, etc. Typischerweise ist es ergänzt mit den Privat- und Fremdschlüsseln und weiteren notwendigen Attributen.

Das **physische Datenmodell** erweitert dann das logische Datenmodell um die produktespezifischen Erzeugungsanweisungen (create table usw., also die 'data definition language', DDL) sowie den besonderen technischen Aspekten wie Indizes, Partitionierung, etc. Zumeist ist dieses Modell dann schon als (SQL-)Skript-Datei vorhanden (oder kann von einer 'Workbench' generiert werden) um die "Definition" in die Datenbank zu importieren.


Die **3 NF Modellierung** findet man hauptsächlich in operativen Systemen, d.h. Systeme in denen die Dateneingaben an sich stattfinden (z.B. ERP Systeme). Darüber hinaus auch im Core DWH oder Enterprise DWH. Dabei sollen die Daten in anwendungsfreier Form gespeichert werden. Das **Star Schema** stellt eine besondere Art der Modellierung dar, die vor allem auf Auswertungssysteme ausgelegt ist. Diese findet man dann eben auch größtenteils im Reporting Layer (Data Mart) eines Datawarehouse Systems. Die **Data Vault Modellierung** ist eine recht neue Form der Modellierung. Diese zielt auf eine einfache Erweiterbarkeit und möglichst automatisierte Erstellung der Beladejobs ab. Man findet diese Form der Modellierung dann auch hauptsächlich im Bereich des Core DWH, Integration Layer, etc.


### Auftrag:

1. Fassen Sie den obigen Text in Stichworten zusammen

2. Schreiben Sie sich die Normalisierungsschritte in das hier geführte Lernportfolio nochmals auf. (Repetition der Vorgehensweise zur [Normalisierung (M162)](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Theorie_Normalisierung.md?ref_type=heads)).

3. Lösen Sie die Aufgabe ['Tourenplaner'](./Tourenplaner/Readme.md) indem Sie zuerst das konzeptionelle Modell ERM auf Papier entwickeln und dann das logische Diagramm ERD in die Workbench übertragen und weiterentwickeln. Dazu brauchen Sie auch die "Normalisierung"

   
![Note](../x_res/note.png) Ablage im Lernportfolio (Scripte und Resultate)

---

![Train](../x_res/Train_D1.png)

## Installation MySQL (MariaDB) und Workbench
   
- [Installation DBMS](./Installation_SW.md) &#10142; **Lokal** **MariaDB** / XAMPP / Docker / AWS

---

![Checkpoint](../x_res/CP.png)

#### Checkpoint

1. Wozu wird Normalisiert?
2. Nenne die Merkmale der drei Modelle.

---

![](../x_res/Buch.jpg)

## Referenzen

[Lernportfolio](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-Portfolio#wahl-eines-tools)

[ICT-Berufsbildung](https://www.modulbaukasten.ch/module/164/1/de-DE?title=Datenbanken-erstellen-und-Daten-einf%C3%BCgen)  
 
[Modul-ID als PDF](../Unterlagen/164_1_Datenbanken_erstellen_und_Daten_einfuegen.pdf)
