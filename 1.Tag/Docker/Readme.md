# Docker installieren

[TOC]

## Linux Shell unter Windows 10/ 11
Das Windows Subsystem für Linux ([WSL](https://de.wikipedia.org/wiki/Windows-Subsystem_f%C3%BCr_Linux)) ist eine gute Möglichkeit, um an eine echte Linux-Shell unter Windows zu kommen.

Wir werden nun WSL 2 einrichten, damit alle die gleiche Ausgangsbasis für zukünftige Übungen haben.
Unter MacOS und Linux brauchen sie diese Schritte natürlich nicht ausführen.

>**Voraussetzungen**<br>
>Stellen Sie sicher, dass Ihr [System alle Voraussetzungen erfüllt](https://learn.microsoft.com/de-de/windows/wsl/install#prerequisites)!


Starten sie eine PowerShell und geben Sie folgenden Befehl ein.

```PowerShell
wsl --status
```
Sie erhalten dann Auskunft über die aktuelle Version von WSL. Die Standardversion sollte auf 2 stehen.<br>
Falls bei Ihnen jedoch die *Version 1* steht, müssen sie ein [WSL-Upgrade](https://learn.microsoft.com/de-de/windows/wsl/install#upgrade-version-from-wsl-1-to-wsl-2) vornehmen.

Aktualisieren Sie ihre WSL-Installation mit


```PowerShell
wsl --update
```
Installieren Sie die aktuelle Ubuntu Distribution mit

```PowerShell
wsl --install ubuntu
```

Bei der Installation werden Sie gebeten, einen Nutzernamen für die Distribution einzugeben. Nutzen Sie am besten den gleichen Nutzernamen, wie sie ihn auch für Windows nutzen.
Ausserdem müssen Sie ein Passwort für Ihren Linux User vergeben.

Danach sollten Sie eine Linux Shell sehen.
Lassen Sie sich die ID der aktuellen Distribution anzeigen. 

```Bash
uname -r 
```
Herzlichen Glückwunsch. Damit haben Sie eine vollständige Linux Distribution auf ihrem Windows-Rechner am Laufen. Schauen Sie sich ein wenig um und probieren sie ein paar Sachen aus.


## Docker Desktop installieren
Wir benötigen noch eine Docker-Umgebung auf unserem PC, damit wir unsere DB im Container ausführen können. Eine gängige Variante dafür ist [Docker Desktop](https://docs.docker.com/desktop/).

Installieren Sie Docker Desktop nach dieser [Anleitung](https://docs.docker.com/desktop/install/windows-install/). Halten Sie sich dabei an die Anweisungen für "WSL 2 Backend" und nicht für Hyper-V.

Sie können Docker Desktop leider nicht ohne Weiteres ausführen, falls Sie nicht als Adminitrator auf Ihrem PC eingelogt sind.
Um Docker auch als Non-Root ausführen zu können, müssen Sie ihren aktuellen user der Gruppe docker-users hinzufügen.

>**Wichtig:**
>
>Damit Sie auf den docker-Befehl in ihrer Ubuntu-Shell zugreifen können, müssen Sie noch dafür sorgen, dass in Docker-Desktop unter *Einstellungen -> Resources -> WSL Integration* das Häckchen bei "Enable integraton with my default WSL distro" ausgewählt ist.

---
>**Optional**:
>
>Die erforderlichen Berechtigungen zum Arbeiten mit Docker-Containern haben Sie nur, wenn Sie Mitglied der Gruppe „docker-users“ sind. Führen Sie die folgenden Schritte aus, um sich selbst unter Windows 10 oder höher der Gruppe hinzuzufügen:
>
>1. Öffnen Sie über das Startmenü die Computerverwaltung.
>2. Erweitern Sie Lokale Benutzer und Gruppen, und klicken Sie auf Gruppen.
>3. Suchen Sie die Gruppe docker-users, klicken Sie mit der rechten Maustaste darauf, und klicken Sie auf Zur Gruppe hinzufügen.
>4. Fügen Sie Ihr Benutzerkonto oder Ihre Benutzerkonten hinzu.
>5. Melden Sie sich zuerst ab und dann wieder an, damit die Änderungen wirksam werden.

##	MySQL Datenbank mit docker-compose vorbereiten
Docker-compose ist ein Tool, mit dem Sie Anwendungsumgebungen mit mehreren Containern bereitstellen können. Docker-compose basiert auf [YAML](https://yaml.org/)(Benutzerfreundliches und lesbares Datenserialisierungsformat).
Laden Sie das [vorbereitete YAML file](docker-compose/docker-compose.yml) herunter und legen Sie es in einem lokalen Projektordner ab. 

Öffnen Sie nun ihr *linux shell* und wechseln Sie nun in den lokalen Projektordner.

>Hinweis: Linux unter Windows wird das C-Verzeichnis gemounted. Es befindet sich unter */mnt/c*

![](../../x_res/linux_shell_projektordner.png)

Führen Sie folgenden Befehl aus:
```bash
docker-compose up -d
```
Mit dem docker-compose file wird nun eine mySql-Datenbank Image (Containervorbereitung) vom docker-hub (repository) heruntergeladen und dabei diese image als container gestartet.




---
Quellen:<br>
[WSL Befehle](https://learn.microsoft.com/de-de/windows/wsl/basic-commands)<br>
[Linux Shell Befehle](https://wiki.ubuntuusers.de/Shell/Befehls%C3%BCbersicht/)



