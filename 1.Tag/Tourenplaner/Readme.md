# Auftrag Tourenplaner

Ein Busunternehmen beschäftigt Disponenten und Fahrer. Die Disponenten planen und organisieren die Fahrten. Eine Fahrt hat mehrere Stationen, wobei Orte nur einmal angefahren werden dürfen. An jeder Station kann der Fahrer wechseln. Pro Station kann eine Ankunfts- und eine Abfahrtszeit erfasst werden, wobei für die Start-Station nur Abfahrtszeit erfasst wird und für die Ziel-Station nur die Ankunftszeit. Fahrer und Disponenten sind über Telefon erreichbar. Jeder Fahrt wird jeweils ein Fahrzeug zugeordnet, welches eine gewisse Anzahl Sitzplätze zur Verfügung stellt. 

![Skizze](./Tourenplaner.jpg)

Die Fahrten sind in einem Excel-Sheet registriert. Ein Beispiel wie dies bis jetzt mit Excel erfasst wurde finden sie im File [Tourenplaner_Daten.xlsx](./Tourenplaner_daten.xlsx).

## Aufgaben

1.	Analysieren Sie die obige Anforderung und zeichnen Sie ein konzeptionelles Datenmodell in draw.io (oder mit Papier und Bleistift)
    -	Das Modell enthält nur Entitätstypen (Tabellen) und Beziehungen

2.	Setzen Sie ihre Entwicklung fort, indem Sie ein logisches Datenmodell im MySQL-Workbench zeichnen.
    -	Beschreiben Sie die notwendigen Tabellen und Beziehungen
    -	Bringen sie ihr Datenmodell in die 3. Normalform (Normalisierung von Datenbanken)

Zeit: 60 Min.
Form: 2er / 3er Team
