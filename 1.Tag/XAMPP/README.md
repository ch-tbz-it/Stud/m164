![TBZ Logo](../../x_res/tbz_logo.png)
![m319 Picto](./x_res/DB-System.png)

[TOC]

# XAMPP installieren (mysql.exe, phpMyAdmin, MariaDB)

![](./x_res/Train_D1.png) 

Es gibt ein MySQL-Variante, die für die Entwicklung einfacher ist und die wir uns hier genauer anschauen wollen: Das Bundle **XAMPP**: (sprich 'Schamp')

Dabei handelt es sich um ein so genanntes Bundle aus den Produkten **A**pache als Webserver, **M**ySQL als Datenbank und den Skriptsprachen **P**erl und **P**HP. Das **X** stand "mal" nur für die Windows-Version ergo gab es auch eine **L**AMPP und sogar eine **M**AMPP Version (Linux und Mac). Aktuell heissen alle Bundles XAMPP!

![](./x_res/ToDo.png) Sie sollten auf Ihrem Computer oder einer virtuellen Umgebung das **XAMPP-Bundle** mit den im Bild angegebenen Komponenten installiert. Die aktuelle XAMPP-Version 8.X.Y finden Sie unter <https://www.apachefriends.org/de/index.html> – inklusive Dokumentation und Anleitung. 
Die Installation ist ausreichend dokumentiert &#8594; konsultieren Sie also das [Manual](https://www.apachefriends.org/docs/). [Video](https://www.youtube-nocookie.com/embed/h6DEDm7C37A)

*Hinweis: Allenfalls müssen Sie vorab noch das neuste [C++ Redistributable Package](https://www.der-windows-papst.de/2020/07/24/download-microsoft-visual-c-redistributable-alle-versionen/) installieren.*

![](./x_res/WarnungXAMPP.png)

![](./x_res/Komponenten.png)

Abb.: Warnung (ignorieren!) & XAMPP Setup Auswahl

![](./x_res/Hinweis.png) Empfohlener Pfad, der in diesen Unterlagen verwendet wird: `C:\xampp\`
(Durch das Löschen dieses Pfades kann XAMPP einfach deinstalliert werden!)

![](./x_res/InstallFolder.png)

Wir verwenden die **MariaDB** als Datenbankmanagementsystem. Hier etwas [Geschichte](https://www.informatik-aktuell.de/betrieb/datenbanken/mariadb-und-mysql-vergleich-der-features.html) und warum der DB-Server so heisst. Hinter der Datei **mysqld.exe** verbirgt sich das DBMS. 

Die Klienten **phpMyAdmin** und **mysql.exe** sind im XAMPP-Bundle bereits enthalten und stellen eine Alternative zur Workbench dar. 
	
> ![](./x_res/caution.png) **Achtung**: XAMPP ist zur Entwicklung konstruiert. Daher sind die Sicherheitsvorkehrungen bewusst ausgeschaltet, um bei der Entwicklung und beim Testen (gute Informatiker machen das!) ohne zusätzliche Hürden arbeiten zu können. Ein produktiver Server sollte niemals mit dem XAMPP-Bundle aufgesetzt werden. Im Juli 2011 wurde das Observationssystem „PATRAS“ der deutschen Bundespolizei vom Hacker-Netzwerk „no name crew“ (kurz „nn-Crew) gehackt. Ziemlich peinlich, denn dieses System enthielt sensible Daten und wurde mittels eines XAMPP-Bundles auf einem Server betrieben.

> **Hinweis**: Bei **MS MYSQL** ist es wichtig, dass es die Version 8.0.26 oder älter ist, damit man die Option "Use SSL" auf "if available" setzen kann. Genauere Anleitung um "SSL" auf "if available" zu setzen finden Sie bei der verifizierten [Antwort von IT Thug Ninja.](https://dba.stackexchange.com/questions/199154/mysql-workbench-ssl-is-required-but-the-server-doesnt-support-it)

## Umgebungsvariable %Path setzen

Setzen Sie am besten noch die Path-Systemvariable. Dann können Sie die MySQL-Tools jeweils direkt ohne Pfad starten!

![](x_res/Umgebungsvariable.png)


---

### DB-Server Manuals
[MariaDB Server Documentation](https://mariadb.com/kb/en/documentation/)

[MySQL Server Documentation](https://dev.mysql.com/doc/)

### phpMyAdmin Manual & Tutorials:
Die Erstellung der Datenbanken und Tabellen und deren Pflege sind im phpMyAdmin ohne Kenntnis von SQL möglich. Es können auch Daten erfasst und modifiziert werden (Funktion „Einfügen“ – wenn eine konkrete Tabelle ausgewählt wurde).

[phpMyAdmin Dokumentation](https://docs.phpmyadmin.net/de/latest/)

[Youtube phpMyAdmin Tour and Beginner's Guide](https://www.youtube.com/watch?v=XEGxz1Undgw)

---


### Probleme beim Starten von XAMPP

> Port 3306 belegt: [Lösung 1](https://stackoverflow.com/questions/16581709/problems-connecting-to-port-3306-mysql-workbench-with-xampp), [Lösung 2](https://stackoverflow.com/questions/6136389/cant-start-mysql-port-3306-busy)

> Port 80 belegt: [Lösung 3](https://www.thecoachsmb.com/blocked-port-error-xampp/)

> MySQL Server startet nicht und im LogFile steht, InnoDB-Engine konnte nicht starten: [Lösung 4](https://stackoverflow.com/questions/33109315/unknown-unsupported-storage-engine-innodb).	Delete `c:\XAMPP\MySQL\DATA\ib_logfile0` und `c:\XAMPP\MySQL\DATA\ib_logfile1`!

---

### MariaDB auf 10.11.x upgraden (NICHT 11.x ! )

Diese Operationen sind sehr heikel. Bitte genau befolgen!

1. XAMPP Control Panel als **Administrator** starten
2. Apache & MySQL **stoppen** und allenfalls **Dienst beenden**: Actions >> [Stopp], Service >> [X]  <br> ![Dienst](./x_res/MySQL_as_Service.png) <br> Darauf auch (alle) XAMPP-ControlPanel beenden!
3. MariaDB Version **10.11.x** als ZIP-Datei [herunterladen](https://mariadb.org/download/?t=mariadb&p=mariadb&r=10.11.2&os=windows&cpu=x86_64&pkg=zip&m=mva)
4. Datei **entzippen** und *darin* alle Ordner und Dateien nach `C:\XAMPP\mysql` verschieben (und überschreiben).
5. Im Verzeichnis `C:\XAMPP\mysql\data` alle *Dateien* **AUSSER** `*.ini` und `*.info` löschen. <br> **ACHTUNG: Dabei keine Unterordner löschen!**
6. MySQL-Dienst via XAMPP-ControlPanel als Administrator wieder starten: Version im Status testen &rarr; 10.11.x
7. Im CMD oder Powershell folgenden Befehl ausführen: `C:\XAMPP\mysql\bin\mariadb-upgrade.exe -u root -p` 
8. Fehlermeldungen ignorieren


---

# XAMPP MySQL-Server starten via Command Line Tool

Wenn Sie auf Ihrem Computer XAMPP installiert haben, finden Sie im Verzeichnis von `C:\xampp\mysql\bin` diverse Startroutinen. Der Server kann direkt von der Betriebssystem-Konsole  (cmd / Powershell / bash) gestartet werden. Mit der Eingabe 

`c:\xampp\mysql\bin\mysqld --verbose –-help` 

können Sie sich sämtliche Optionen und konfigurierbare Systemvariablen anzeigen lassen (oder Sie schauen im MySQL-Manual nach).

![](./x_res/Wichtig.png) Falls Sie durch eine Übung oder aus Versehen die Berechtigungen des **root**-Users verstellt haben und keine Berechtigungen mehr haben diese zu ändern, können Sie den Datenbankserver mit dem Befehl 

`c:\xampp\mysql\bin\mysqld --skip-grant-tables`

aufstarten. Damit haben alle Benutzer alle Rechte. Es ist also eine sehr heikle Operation. Aber so haben Sie wieder Zugriff auf die Berechtigungsdatenbank und können die Berechtigungen korrigieren.


# XAMPP MySQL-Server starten via Control Panel

Der Server muss laufen, damit Sie von einem Klienten mit der Datenbank kommunizieren können – sei es als Prozess oder als Service.

Eine einfache Startmöglichkeit ist das **XAMPP-Control-Panel**. Skeptische Geister können den Erfolg auch im Task-Manager überprüfen. Manchmal liegt das Problem beim Start auch daran, dass der MySQL-Server bereits gestartet ist. Ein zweiter Start schlägt dann fehl. Sie sollten dieses Verhalten mit den verschiedenen Start-Prozeduren testen.

> ![](./x_res/Wichtig.png) Das XAMPP-Controlpanel sollten Sie grundsätzlich als **Administrator** starten, das für die Windows-Dienste Zugriffe auf die Registry nötig sind. Für den Start als Service und weitere Optionen also unumgänglich.


### Als Prozess starten

![CP als Prozess](./x_res/XAMPP_CP.png)

Es lohnt sich mit den verschiedenen Startoptionen zu experimentieren. Im Control Panel ist der Status jeweils sichtbar. Beenden Sie den **Prozess** zum Beispiel im Task-Manager und starten Sie den Server direkt indem Sie die Datei `mysqld.exe` im `bin`-Verzeichnis ausführen.

### Als Service starten


Sie können MySQL auch als **Service** installieren (markieren der Checkbox `Svc` im Control-Panel). Der Server wird dann in die Diensteliste der Computerverwaltung eingetragen und kann so konfiguriert werden, dass er bei *Systemstart automatisch gestartet* wird. 

![CP als Service](./x_res/XAMPP_CP_Service.png)

In diesem Fall kann der Server auch über den Dienst-Manager von Windows gestartet und gestoppt werden oder aus einer Windows-Konsole (cmd / Powershell) heraus:

```BATCH
C:\>NET STOP mysql
   The MySql service is stopping.
   The MySql service was stopped successfully.

C:\>NET START mysql
   The MySql service is starting.
   The MySql service was started successfully.
```

### Konfiguration über das Control Panel starten

    Aktionen >>> MySQL >>> [Konfig]

### phpMyAdmin über das Control Panel starten

    Aktionen >>> MySQL >>> [Admin]
    
---

<br>

---

## Optionale Installation: HeidiSQL (WIN)

**HeidiSQL** ist eine freie Software, die leicht zu erlernen ist. Mit "Heidi" können Sie Daten und Strukturen von Computern, auf denen eines der Datenbanksysteme MariaDB, MySQL, Microsoft SQL, PostgreSQL und SQLite läuft, sehen und bearbeiten. Erfunden im Jahr 2002 von Ansgar, gehört HeidiSQL zu den beliebtesten Tools für MariaDB und MySQL weltweit. Normalerweise wird HeidiSQL mit der MariaDB Version von Windows zusammen installiert.

Der Klient kann separat als [EXE oder als portable Version](https://www.heidisql.com/download.php) installiert werden.

---

### HeidiSQL Tutorials:
[Youtube MariaDB Tutorial For Beginners in One Hour](https://www.youtube.com/watch?v=_AMj02sANpI)

---

## Optionale Installation: Sequel Pro (Mac)

Sequel Pro ist ein schneller, benutzerfreundlicher Klient zur Verwaltung von MySQL-Datenbanken.

### Sequel Pro Manual & Tutorials:

[Sequel Pro Manual](https://sequelpro.com/docs)

[Youtube Installing sequel pro for database connections](https://www.youtube.com/watch?v=66jPBqWp0jI)








