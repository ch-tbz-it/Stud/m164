![](../x_res/tbz_logo.png)

# M164 Lösungen 5.Tag


![Checkpoint](../x_res/CP.png)


### Nennen Sie die fünf Aspekte der **Datenintegrität** in einer Datenbank.

1. **Eindeutigkeit und Konsistenz**: Jeder Datensatz in der Datenbank sollte eindeutig identifizierbar und dauerhaft sein. **&rarr; ID (PK)**

2. **Referenzielle Integrität**: Wenn eine Tabelle Beziehungen zu anderen Tabellen hat, sollten die Beziehungen immer konsistent bleiben. **&rarr; FK-Constraint, NN, UQ**

3. **Datentypen**: Die Daten sollten in der Datenbank in den korrekten Datentypen gespeichert werden.

4. **Datenbeschränkungen**: Datenbeschränkungen stellen sicher, dass die Daten in der Datenbank gültig sind. **&rarr; CHECK**

5. **Validierung**: Vor dem Einfügen von Daten in die Datenbank sollte eine Validierung durchgeführt werden. **&rarr; 3.NF, Testen auf Korrektheit und Redundanz, GUI-Pulldown**

Datenintegrität und Datenkonsistenz sind zwei wichtige Konzepte im Bereich der Datenverwaltung. Sie beziehen sich auf verschiedene Aspekte der Datenqualität und -sicherheit.

### Was ist der Unterschied zwischen Datenintegrität und Datenkonsistenz?

- **Datenintegrität** bezieht sich auf die Korrektheit, Vollständigkeit und Konsistenz von Daten¹. Sie stellt sicher, dass die in einer Datenbank gespeicherten Informationen dauerhaft korrekt, vollständig und vertrauenswürdig bleiben, ganz gleich, wie oft auf sie zugegriffen wird¹. Datenintegrität konzentriert sich auf die Aufrechterhaltung der Genauigkeit, Konsistenz und Zuverlässigkeit von Daten⁶. Es gibt verschiedene Arten der Datenintegrität, darunter physikalische und logische Datenintegrität¹.

- **Datenkonsistenz** hingegen bezieht sich auf die Fähigkeit, nur festgeschriebene Daten zu einem bestimmten Zeitpunkt zu lesen, nicht die Zwischenschritte⁴. Es geht darum, sicherzustellen, dass alle Daten in einem System zu jedem Zeitpunkt einen konsistenten Zustand aufweisen.

 Zusammenfassend lässt sich sagen, dass Datenintegrität sich auf die Genauigkeit und Vollständigkeit der Daten konzentriert, während Datenkonsistenz sicherstellt, dass die Daten über die Zeit hinweg konsistent bleiben. Beide sind für die Aufrechterhaltung der Datenqualität und -sicherheit von entscheidender Bedeutung.


### Was ist die **Gefahr** bei der FK-Constraint-Option `ON DELETE Cascade`?

1. **Unbeabsichtigtes Löschen von Daten**: Wenn Sie nicht vorsichtig sind, können Sie durch das Löschen eines einzigen Datensatzes eine Kaskade von Löschvorgängen auslösen, die sich auf mehrere Tabellen ausbreiten und zu einem unbeabsichtigten Datenverlust führen.

2. **Komplexität**: Die Verwendung von `ON DELETE CASCADE` kann die Komplexität Ihrer Datenbank erhöhen und es schwieriger machen, die Auswirkungen von Löschvorgängen zu verstehen.

3. **Leistungsprobleme**: Das kaskadierende Löschen kann zu Leistungsproblemen führen, insbesondere wenn grosse Mengen von Daten betroffen sind.



### Was ist der **Unterschied** zwischen `COUNT(*)` und `COUNT(attr)`?
  
  - `COUNT(*)` zählt ALLE Datensätze an, auch solche mit `NULL`-Werten.
  -  `COUNT(attr)` zählt Datensätze, die in der Spalte `attr` Werte ≠ `NULL` drin haben. 
  
### Formuliere einen SELECT-Befehl mit der `WHERE BETWEEN` Klausel. ([Tipp](https://www.w3schools.com/sql//sql_between.asp))

```SQL
 SELECT * FROM Products
 WHERE Price BETWEEN 10 AND 19;
```
*Hinweis: Preiswerte `10` und `19` werden auch angezeigt!*

### Worauf müssen Sie bei der **HAVING** Klausel achten?

1. **Verwendungszweck**: Die `HAVING` Klausel wird verwendet, um Daten nach bestimmten Kriterien zu filtern. Sie wird üblicherweise bei der Erstellung von Berichten verwendet.
	
2. **Verwendung mit GROUP BY**: Die `HAVING` Klausel wird oft in Verbindung mit der `GROUP BY` Klausel verwendet. Wenn Sie die `GROUP BY` Klausel verwenden, ordnen Sie Datensätze in Gruppen an, um Aggregationswerte (Statistiken) für sie zu berechnen. `HAVING` filtert Datensätze nach diesen Aggregatwerten.
	
3. **Unterschied zu WHERE**: Die `HAVING` Klausel wurde zu SQL hinzugefügt, weil das `WHERE` Schlüsselwort nicht mit Aggregatfunktionen verwendet werden kann.
		
Bitte beachten Sie, dass die Beherrschung dieser Klausel es SQL-Benutzern ermöglicht, wertvolle Erkenntnisse aus aggregierten Daten zu gewinnen.
	

### Formulieren Sie einen Satz, der den **Einsatz** von Subqueries erklärt und begründet.

  - In SQL werden Unterabfragen (Subqueries) verwendet, um komplexe Abfragen zu erstellen und zu vereinfachen, indem sie es ermöglichen, mehrere Abfragen in einer einzigen Abfrage auszuführen, wodurch die Notwendigkeit mehrerer Abfragen und die damit verbundene Verarbeitungszeit reduziert wird. 
  - Sie können auch dazu verwendet werden, um Daten zu filtern, die auf Ergebnissen basieren, die von einer anderen Abfrage zurückgegeben werden, was die Flexibilität und Effizienz der Datenmanipulation erhöht.