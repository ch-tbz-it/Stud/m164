-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0; -- !!
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='';

-- -----------------------------------------------------
-- Schema Freifaecher
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `Freifaecher` ;

-- -----------------------------------------------------
-- Schema Freifaecher
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Freifaecher` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `Freifaecher` ;

-- -----------------------------------------------------
-- Table `tbl_LehrerIn`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_LehrerIn` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tbl_LehrerIn` (
  `ID_LehrerIn` INT NOT NULL,
  `Vorname` VARCHAR(30) NOT NULL,
  `Nachname` VARCHAR(30) NOT NULL,
  `Geb_datum` DATE NULL,
  PRIMARY KEY (`ID_LehrerIn`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `tbl_Klasse`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_Klasse` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tbl_Klasse` (
  `ID_Klasse`  VARCHAR(5) NOT NULL,     -- Die ID trägt hier Informationen!! Ist in der Folge dann etwas einfacher ...
  `FK_KlassenLehrerIn` INT NOT NULL,
  PRIMARY KEY (`ID_Klasse`),
  INDEX `fk_tbl_Klasse1_idx` (`FK_KlassenLehrerIn` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_Klasse1`
    FOREIGN KEY (`FK_KlassenLehrerIn`)
    REFERENCES `tbl_LehrerIn` (`ID_LehrerIn`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `tbl_SchülerIn`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_SchülerIn` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tbl_SchülerIn` (
  `ID_SchülerIn` INT NOT NULL AUTO_INCREMENT,
  `Vorname` VARCHAR(30) NOT NULL,
  `Nachname` VARCHAR(30) NOT NULL,
  `Geb_datum` DATE NOT NULL,
  `FK_Klasse` VARCHAR(5) NOT NULL,  -- Die ID trägt hier Informationen!! --> CASCADE
  PRIMARY KEY (`ID_SchülerIn`),
  INDEX `fk_tbl_SchülerIn1_idx` (`FK_Klasse` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_SchülerIn1`
    FOREIGN KEY (`FK_Klasse`)
    REFERENCES `tbl_Klasse` (`ID_Klasse`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)   -- Wegen ID mit Informationen!! --> CASCADE
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `tbl_Freifach`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_Freifach` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tbl_Freifach` (
  `ID_Freifach` INT NOT NULL,
  `Freifach` VARCHAR(30) NOT NULL,
  `Tag` VARCHAR(2) NOT NULL,
  `Zimmer` VARCHAR(10) NOT NULL,
  `FK_FF_LehrerIn` INT NOT NULL,
  PRIMARY KEY (`ID_Freifach`),
  INDEX `fk_tbl_Freifach1_idx` (`FK_FF_LehrerIn` ASC) VISIBLE,
  UNIQUE INDEX `FK_FF_LehrerIn_UNIQUE` (`FK_FF_LehrerIn` ASC) VISIBLE,
  CONSTRAINT `fk_tbl_Freifach1`
    FOREIGN KEY (`FK_FF_LehrerIn`)
    REFERENCES `tbl_LehrerIn` (`ID_LehrerIn`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `TT_Teilname`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tt_Teilnahme` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tt_Teilnahme` (
  `FK_Freifach` INT NOT NULL,
  `FK_SchülerIn` INT NOT NULL,
  PRIMARY KEY (`FK_Freifach`,`FK_SchülerIn`),
  INDEX `fk_tt_Teilnahme1_idx` (`FK_Freifach` ASC) VISIBLE,
  INDEX `fk_tt_Teilnahme2_idx` (`FK_SchülerIn` ASC) VISIBLE,
  CONSTRAINT `fk_tt_Teilnahme1`
    FOREIGN KEY (`FK_Freifach`)
    REFERENCES `tbl_Freifach` (`ID_Freifach`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_tt_Teilnahme2`
    FOREIGN KEY (`FK_SchülerIn`)
    REFERENCES `tbl_SchülerIn` (`ID_SchülerIn`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
