use freifaecher;

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;  -- Keine Fehlermeldungen beim einseitigen Erstellen von Beziehungen
-- ----------
-- Aufgabe 7: Einfügen der 290 DS
-- ----------


LOAD DATA LOCAL INFILE 'C:/Users/michael/Desktop/290_SchuelerInnen.csv' 
	REPLACE                   -- Eliminiert doppelte DS!
    INTO TABLE tbl_SchülerIn
	CHARACTER SET utf8mb4
	FIELDS TERMINATED BY ';'
	LINES TERMINATED BY '\r\n'
	IGNORE 1 ROWS
	(ID_SchülerIn, Nachname, Vorname, @Geburtsdatum, @Klassennummer, @Ff1, @Ff2)  -- Nur SchülerInnen einlesen und Klasse anpassen
    SET Geb_datum = STR_TO_DATE(@Geburtsdatum, '%d.%m.%y'),  
		FK_Klasse = CASE                    -- Weil die ID diese Info trägt, muss nun angepasst werden!
		WHEN @Klassennummer = 1 THEN '11a'
		WHEN @Klassennummer = 2 THEN '11b'
		WHEN @Klassennummer = 3 THEN '12a'
		WHEN @Klassennummer = 4 THEN '12b'
		ELSE '12c'
	END;
Select * FROM tbl_schülerin;

-- Spalte FreifachNr1 einlesen: Leere FreifachNr1 werden ignoriert!

LOAD DATA LOCAL INFILE 'C:/Users/michael/Desktop/290_SchuelerInnen.csv' 
	REPLACE                   -- Eliminiert doppelte DS!
    INTO TABLE tt_Teilnahme
	CHARACTER SET utf8mb4
	FIELDS TERMINATED BY ';'
	LINES TERMINATED BY '\r\n'
	IGNORE 1 ROWS
	(@ID_SchuelerIn, @Nachname, @Vorname, @Geburtsdatum, @Klassennummer, @Ff1, @Ff2)  -- Nur FKs einlesen und anpassen
    SET FK_SchülerIn = @ID_SchuelerIn,  
		FK_Freifach = IF(@Ff1='', 0, @Ff1);  -- Leer-Werte werden mit 0 beschrieben --> DELETE!!

-- Spalte FreifachNr2 einlesen: Leere FreifachNr2 werden ignoriert!
LOAD DATA LOCAL INFILE 'C:/Users/michael/Desktop/290_SchuelerInnen.csv' 
	REPLACE                   -- Eliminiert doppelte DS!
    INTO TABLE tt_Teilnahme
	CHARACTER SET utf8mb4
	FIELDS TERMINATED BY ';'
	LINES TERMINATED BY '\r\n'
	IGNORE 1 ROWS
	(@ID_SchuelerIn, @Nachname, @Vorname, @Geburtsdatum, @Klassennummer, @Ff1, @Ff2)  -- Nur FKs einlesen und anpassen
    SET FK_SchülerIn = @ID_SchuelerIn,  
		FK_Freifach = IF(@Ff2='', 0, @Ff2);  -- Leer-Werte werden mit 0 geschrieben!! --> DELETE!!
 
 --  Bereinigen Sie die Daten (Redundanz, leere Werte, Inkonsistenz). Benutzen Sie dazu die Scripte aus Tag 6! als Vorlage.
-- ----------
 
DELETE FROM tt_Teilnahme WHERE FK_Freifach = 0;  -- 0-Werte werden hier gelöscht!!
Select FK_SchülerIn, FK_Freifach FROM tt_Teilnahme;


-- Durch die 2.Normalform und die obigen Angabe von `REPLACE` gibt es hier keine doppelten Werte mehr!!
SELECT Nachname, Vorname, COUNT(*) as Anzahl FROM tbl_SchülerIn
  GROUP BY Nachname, Vorname
  HAVING COUNT(*) > 1;

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS;  --  Fehlermeldungen wieder aktivieren!! (Alter Wert wieder setzen!)

-- Listen ausgeben zu Testzwecken:
-- ----------

-- Liste Schülerinnen:
SELECT s.ID_SchülerIn, s.Nachname, s.Vorname, s.Geb_datum, k.ID_Klasse AS KLasse, l.Nachname AS Klassenlehrer_NN, l.Vorname AS Klassenlehrer_VN, f.ID_Freifach AS FreifachNr, f.Freifach AS Beschreibung, f.Tag, f.Zimmer
FROM tbl_SchülerIn as s 
JOIN tt_teilnahme AS tt ON s.ID_SchülerIn = tt.FK_SchülerIn
JOIN tbl_Freifach AS f ON tt.FK_Freifach = f.ID_Freifach
JOIN tbl_klasse as k ON s.FK_Klasse=k.ID_Klasse
JOIN tbl_Lehrerin as l ON k.FK_KlassenLehrerIn=l.ID_LehrerIn
ORDER BY s.ID_Schülerin;


-- ----
