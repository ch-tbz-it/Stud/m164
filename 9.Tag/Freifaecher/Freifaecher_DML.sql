use freifaecher;

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;  -- Keine Fehlermeldungen bei einseitigen erstellen von Beziehungen
-- ----------
-- Aufgabe 4: Übertragen Sie die Daten der CSV-Dateien in die normalisierten Tabellen mittels LOAD DATA LOCAL INFILE (direkt oder indirekt). Überwachen Sie die korrekte Übertragung der Beziehungen (FK=ID).
-- ----------

TRUNCATE TABLE tbl_lehrerin;
-- Temporäres Attribut hinzufügen:
ALTER TABLE tbl_lehrerin
ADD Freifach VARCHAR(10);
 
LOAD DATA LOCAL INFILE 'C:/Users/michael/Desktop/FF_Lehrer.csv'
	REPLACE 
	INTO TABLE tbl_Lehrerin
	CHARACTER SET utf8mb4
	FIELDS TERMINATED BY ';'
	LINES TERMINATED BY '\r\n'
	IGNORE 1 ROWS
	(ID_LehrerIn, Vorname, Nachname, @Geburtsdatum, Freifach)
    SET Geb_datum = STR_TO_DATE(@Geburtsdatum, '%d.%m.%Y');  -- Bei leeren DS gibt es Warnung --> NULL
Select * FROM tbl_Lehrerin;


-- EINFACHE temporäre Tabelle `temp` für LDIF erstellen

DROP TABLE IF EXISTS `temp` ;
CREATE TABLE IF NOT EXISTS `temp` (
  `ID` INT, -- don't set PK !
  `VN_S` VARCHAR(30),
  `NN_S` VARCHAR(30),
  `GD_S` DATE,
  `Klasse` VARCHAR(30),
  `VN_LP` VARCHAR(30),
  `NN_LP` VARCHAR(30),
  `FreifachNr` VARCHAR(30),
  `Beschreibung` VARCHAR(30),
  `Tag` VARCHAR(2),
  `Zimmer` VARCHAR(30)
   )
ENGINE = InnoDB;

-- Import into temp table

LOAD DATA LOCAL INFILE 'C:/Users/michael/Desktop/FF_Schueler.csv'
	INTO TABLE temp
	CHARACTER SET utf8mb4
	FIELDS TERMINATED BY ';'
	LINES TERMINATED BY '\r\n'
	IGNORE 1 ROWS
	(ID,VN_S, NN_S, @GD_S, Klasse, VN_LP, NN_LP, FreifachNr, Beschreibung, Tag, Zimmer)
	SET GD_S = STR_TO_DATE(@GD_S, '%d.%m.%Y');  -- Bei leeren DS gibt es Warnung --> NULL
Select * FROM temp;


-- >> tbl_Freifach füllen mit der richtigen FF_Lehrperson aus dem temporären Attribut `Freifach` der Tabelle `tbl_Lehrerin`

Truncate TABLE tbl_freifach;
INSERT INTO tbl_freifach (ID_Freifach, Freifach, Tag, Zimmer, FK_FF_Lehrerin)
  SELECT DISTINCT t.FreifachNr, t.Beschreibung, t.Tag, t.Zimmer, lp.ID_LehrerIn FROM temp as t JOIN tbl_Lehrerin AS lp ON t.Beschreibung=lp.Freifach;
  
ALTER TABLE tbl_Lehrerin DROP COLUMN Freifach; -- Temp. Attribut wieder löschen

SELECT * from tbl_freifach;


--  >> tbl_Klasse mit FK_KL_LP füllen 

TRUNCATE TABLE tbl_klasse;
INSERT INTO tbl_klasse (ID_Klasse, FK_KlassenLehrerIn)  -- Die ID trägt hier Infornationen!!
  SELECT DISTINCT t.Klasse, LP.ID_LehrerIn FROM temp as t JOIN tbl_Lehrerin AS lp ON t.NN_LP=lp.Nachname;
SELECT * from tbl_Klasse;


--  >> tbl_Schüler mit FK_Klasse füllen

TRUNCATE TABLE tbl_schülerin;
INSERT INTO tbl_schülerin (ID_SchülerIn, Vorname, Nachname, Geb_datum, FK_Klasse)
   SELECT DISTINCT t.ID, t.VN_S, t.NN_S, GD_S, t.Klasse FROM temp as t;
SELECT * from tbl_schülerin;

--  >> tbl_Schüler mit FK_Freifach und FK_SchülerIn füllen

TRUNCATE TABLE tt_teilnahme;
INSERT INTO tt_teilnahme (FK_SchülerIn, FK_Freifach)
   SELECT ID, FreifachNr FROM temp;
SELECT FK_SchülerIn, FK_Freifach from tt_teilnahme ORDER BY FK_SchülerIn;


-- ----------
-- Aufgabe 5: Bereinigen Sie die Daten (Redundanz, leere Werte, Inkonsistenz). Benutzen Sie dazu die Scripte aus Tag 6! als Vorlage.
-- ----------

-- Durch die 2.Normalform und die obigen Angabe von `DISTINCT` gibt es hier keine doppelten Werte mehr!!
SELECT Nachname, Vorname, COUNT(*) as Anzahl FROM tbl_SchülerIn
  GROUP BY Nachname, Vorname
  HAVING COUNT(*) > 1;

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS;  --  Fehlermeldungen wieder aktivieren!! (Alter Wert wieder setzen!)

DROP TABLE IF EXISTS `temp` ; -- Temporäre Tabelle wieder löschen

-- ----------
-- Aufgabe 6: Ursprüngliche Listen ausgeben zu Testzwecken:
-- ----------

-- Liste Schülerinnen:
SELECT s.ID_SchülerIn, s.Nachname, s.Vorname, s.Geb_datum, k.ID_Klasse AS KLasse, l.Nachname AS Klassenlehrer_NN, l.Vorname AS Klassenlehrer_VN, f.ID_Freifach AS FreifachNr, f.Freifach AS Beschreibung, f.Tag, f.Zimmer
FROM tbl_SchülerIn as s 
JOIN tt_teilnahme AS tt ON s.ID_SchülerIn = tt.FK_SchülerIn
JOIN tbl_Freifach AS f ON tt.FK_Freifach = f.ID_Freifach
JOIN tbl_klasse as k ON s.FK_Klasse=k.ID_Klasse
JOIN tbl_Lehrerin as l ON k.FK_KlassenLehrerIn=l.ID_LehrerIn
ORDER BY s.ID_Schülerin;

-- Liste Freifach LehrerInnen:
SELECT DISTINCT lp.Vorname, lp.Nachname, lp.Geb_datum, ff.Freifach FROM tbl_Lehrerin AS lp LEFT JOIN tbl_Freifach as ff ON ff.ID_Freifach=lp.ID_Lehrerin;

