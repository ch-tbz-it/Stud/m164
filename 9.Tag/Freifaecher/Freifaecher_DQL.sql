-- ----------
-- Aufgabe 8: Führen Sie folgenden SELECT-Aufgaben auf diese Daten aus:
-- ----------

-- Geben Sie aus, wieviele Teilnehmer Inge Sommer in ihrem Freifach hat.
Select count(FK_SchülerIn) AS `Anzahl Tn bei Fr. Sommer`
FROM tbl_Lehrerin AS l 
JOIN tbl_Freifach AS f ON f.FK_FF_LehrerIn = l.ID_LehrerIn
JOIN tt_Teilnahme AS t ON t.FK_Freifach = f.ID_Freifach
WHERE l.Nachname like "%Sommer%" AND l.Vorname like "%Inge%";

-- Geben Sie eine Liste aller Klassen aus, mit jeweils der Anzahl SchülerInnen (→ FK_SchülerIn) in den Freifächern. 
-- Die Liste soll nach den Klassen sortiert werden.
Select  k.ID_Klasse, f.Freifach, COUNT(t.FK_SchülerIn)
   INTO OUTFILE 'C:/Users/michael/Desktop/Liste_Ff_alle_Kl.csv'
FROM tbl_schülerin as s 
JOIN tt_Teilnahme AS t ON t.FK_SchülerIn = s.ID_SchülerIn
JOIN tbl_Freifach AS f ON t.FK_Freifach = f.ID_Freifach
JOIN tbl_Klasse as k ON s.FK_Klasse = k.ID_Klasse
GROUP BY k.ID_Klasse, f.freifach
ORDER BY f.Freifach;


-- Welche SchülerInnen besuchen das Freifach "Chor" oder "Elektronik"?
Select  s.Vorname, s.Nachname, f.Freifach
   INTO OUTFILE 'C:/Users/michael/Desktop/Liste_Chor_Elektronik.csv'
FROM tbl_schülerin as s 
JOIN tt_Teilnahme AS t ON t.FK_SchülerIn = s.ID_SchülerIn
JOIN tbl_Freifach AS f ON t.FK_Freifach = f.ID_Freifach
WHERE f.ID_Freifach IN (
   SELECT ID_Freifach FROM tbl_Freifach WHERE Freifach like "%Chor%" OR Freifach like "%Elektronik");


-- ---------
-- Aufgabe 9: Speichern Sie die Ausgaben von Punkt 8 in eine Datei.
-- ----------
-- >> Siehe Aufgabe 8 oben

-- -----------
-- Aufgabe 10: Machen Sie ein Backup der Datenbank Freifaecher.
-- -----------

-- >> c:\XAMPP\MYSQL\BIN\mysqldump -u root -p Freifaecher > 'C:/Users/michael/Desktop/ff_dump.sql'