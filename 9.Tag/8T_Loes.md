![](../x_res/tbz_logo.png)

# M164 Lösungen 8.Tag:


# Lösung Freifächer

1. Erstellen Sie die **erste Normalform** der EXCEL-Tabelle und exportieren Sie die Daten als CSV-Dateien. (Atomare Felder, Redundanz wird bewusst erzeugt). [EXCEL 1NF in 2 Arbeitsblätter](./Freifaecher/Freifaecher1NF.xlsx)
2. Erstellen Sie das [log. ERD](./Freifaecher/Freifaecher_logERD.png). Die (5) Tabellen müssen mind. in der 2.NF sein. Bestimmen Sie die Kardinalitäten. 
3. Erstellen Sie das [physische ERD](./Freifaecher/Freifaecher_physERD.png) ([&rarr; mwb-Datei](./Freifaecher/Freifaecher.mwb)). Erzeugen Sie durch z.B. Forward Engineering ein [SQL-DDL-Script](./Freifaecher/Freifaecher_DDL.sql).
4. **Übertragen** Sie die Daten der CSV-Dateien in die normalisierten Tabellen mittels `LOAD DATA LOCAL INFILE` (direkt oder indirekt). Überwachen Sie die korrekte Übertragung der Beziehungen (FK=ID).
5. **Bereinigen** Sie die Daten (Redundanz, leere Werte, Inkonsistenz). (Siehe Kommentar [SQL-DML-Script](./Freifaecher/Freifaecher_DML.sql)) 
6. **Testen** Sie die eingelesenen Daten! Vergleichen Sie die Tabellenwerte der CSV-Dateien mit den gleichen, angepassten *Select-Abfragen aus ihrer normalisierten Datenbank*. [SQL-DML-Script](./Freifaecher/Freifaecher_DML.sql)! 
7. Erzeugen Sie weitere **290 Datensätze** an SchülerInnen (Name, Geb.datum, Klassennummer, Freifachnummer) mit einem Testdatengenerator (evtl. geeignete Datenwerte anpassen, bzw. durchmischen in EXCEL). <br> [290 DS](./Freifaecher/290_SchuelerInnen.csv)<br> Führen Sie die obigen Schritte von Punkt 4,5 und 6 erneut durch. [SQL-DML-290-Script](./Freifaecher/Freifaecher_DML_290.sql)
8. Führen Sie folgenden **SELECT-Aufgaben** auf diese Daten aus: <br>
   - Geben Sie aus, wieviele Teilnehmer Inge Sommer in ihrem Freifach hat.
   - Geben Sie eine Liste aller Klassen aus, mit jeweils der Anzahl SchülerInnen (&rarr; FK_SchülerIn) in den Freifächern. Die Liste soll nach den Klassen sortiert werden. 
   - Welche SchülerInnen besuchen das Freifach "Chor" oder "Elektronik"?
9. **Speichern** Sie die Ausgaben von Punkt 8 **in eine Datei**. Benutzen Sie den Befehl `SELECT INTO OUTFILE` dazu. [SQL-DQL-Script](./Freifaecher/Freifaecher_DQL.sql) <br> [Siehe Manual](https://mariadb.com/kb/en/select-into-outfile/) 
10. Machen Sie ein **Backup** der Datenbank Freifaecher.


# Lösung Opendata Steuerdaten

1. CSV analysieren und nach Redundanzen suchen.
2. Werte wiederholen sich (redundant) und weisen auf NF2 hin.
3. Gem. 2 Normalfall Regel ergeben sich somit insgesamt 4 Tabellen <br> ![](./Opendata/Normalisieren_Steuern.png)
4. Folgende Tabellen entstehen:
   - Jahr
   - Quartier
   - Steuertarif
   - Steuern <br>       ![](./Opendata/Steuern_ERD.png)
5. Physisches Modell erstellen, siehe [ERD Modell](./Opendata/Steuern.mwb)
   
6. Das ursprüngliche CSV File aufteilen in vier Files (Redundanzen entfernen, z.B. in Excel mit *Duplikate entfernen* oder via SQL &rarr; 6.Tag)
   - Pro CSV File gem. DDL Definition anpassen, sodass Bulkimport ohne Fehler durchläuft.
   - Achtung: Die FK's müssen manuell oder über die ID der CSV-Dateien eingefügt werden! <br> [Dateien](./Opendata/)

4.	Lösung der drei Fragen:<br>
    a.	Ermitteln Sie das Quartier mit maximalem Steuereinkommen für \_p75: **Fluntern: 77352** <br>
    b.	Welches Quartier hat das niedrigste Steuereinkommen für \_p50?: **Hard: 516** <br>
    c.	Welches Quartier hat das höchste Steuereinkommen für \_p50?: **Fluntern: 20936** <br>
       