-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema opendata
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema opendata
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `opendata` DEFAULT CHARACTER SET utf8 ;
USE `opendata` ;

-- -----------------------------------------------------
-- Table `opendata`.`Jahr`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `opendata`.`Jahr` (
  `Jahr_id` INT NOT NULL AUTO_INCREMENT,
  `Stichtagjahr` DATE NOT NULL,
  PRIMARY KEY (`Jahr_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `opendata`.`Quartier`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `opendata`.`Quartier` (
  `Quartier_id` INT NOT NULL AUTO_INCREMENT,
  `Sortierung` INT NOT NULL,
  `cd` INT NOT NULL,
  `Quartiername` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Quartier_id`),
  UNIQUE INDEX `Quartiername_UNIQUE` (`Quartiername` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `opendata`.`Steuertarif`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `opendata`.`Steuertarif` (
  `Steuertarif_id` INT NOT NULL AUTO_INCREMENT,
  `Sortierung` INT NOT NULL,
  `cd` INT NOT NULL,
  `Tarifname` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Steuertarif_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `opendata`.`Steuern`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `opendata`.`Steuern` (
  `Steuern_id` INT NOT NULL AUTO_INCREMENT,
  `FK_Quartier_id` INT NOT NULL,
  `FK_Steuertarif_id` INT NOT NULL,
  `FK_Jahr_id` INT NOT NULL,
  `Steuereinkommen_p50` FLOAT NULL,
  `Steuereinkommen_p25` FLOAT NULL,
  `Steuereinkommen_p75` FLOAT NULL,
  PRIMARY KEY (`Steuern_id`),
  INDEX `fk_Steuern_Quartier_idx` (`FK_Quartier_id` ASC) VISIBLE,
  INDEX `fk_Steuern_Steuertarif1_idx` (`FK_Steuertarif_id` ASC) VISIBLE,
  INDEX `fk_Steuern_Jahr1_idx` (`FK_Jahr_id` ASC) VISIBLE,
  CONSTRAINT `fk_Steuern_Quartier`
    FOREIGN KEY (`FK_Quartier_id`)
    REFERENCES `opendata`.`Quartier` (`Quartier_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Steuern_Steuertarif1`
    FOREIGN KEY (`FK_Steuertarif_id`)
    REFERENCES `opendata`.`Steuertarif` (`Steuertarif_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Steuern_Jahr1`
    FOREIGN KEY (`FK_Jahr_id`)
    REFERENCES `opendata`.`Jahr` (`Jahr_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
