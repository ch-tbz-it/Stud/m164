![TBZ Logo](../x_res/tbz_logo.png)
![m164 Picto](../x_res/m164_picto.jpg)



# m164 - Datenbanken erstellen und Daten einfügen

[TOC]

---

# Tag 9

> ![Teacher](../x_res/Teacher.png) <br> Recap / Q&A Tag 8  <br>
> [Lösung 8.Tag](8T_Loes.md)

---



---

## Common Table Expressions CTEs

> ![Teacher](../x_res/Teacher.png) <br> 

CTE steht für Common Table Expressions und ist ein Konzept, das in vielen relationalen Datenbanksystemen verwendet wird, um Abfragen zu vereinfachen und die Lesbarkeit von SQL-Statements zu erhöhen. Eine CTE ist eine temporäre Ergebnismenge, die innerhalb einer SELECT, INSERT, UPDATE oder DELETE Abfrage definiert wird. Sie wird direkt vor der Hauptabfrage definiert und kann dann innerhalb der Abfrage referenziert werden.


Eine CTE-Tabelle hat keinen dauerhaften Bestand in der Datenbank, sondern existiert nur für die Dauer der Abfrage, in der sie definiert wurde. Sie kann verwendet werden, um komplexe Abfragen zu vereinfachen, wiederholte Berechnungen zu vermeiden und eine bessere Strukturierung zu ermöglichen.

![Learn](../x_res/Learn.png)

Eine CTE wird mit der WITH-Klausel eingeleitet, gefolgt von der Definition der CTE. Danach kann die CTE wie eine normale Tabelle in der Hauptabfrage verwendet werden.

```
WITH MitarbeiterAbteilung AS (
    SELECT Name, Abteilung
    FROM Mitarbeiter
    WHERE Abteilung = 'IT'
)
SELECT Name
FROM MitarbeiterAbteilung;

```




### Auftrag 

![ToDo](../x_res/ToDo.png) *Zeit: ca. 75 Min*

![Learn](../x_res/Train_R1.png)

Probieren die CTEs in Ihrem Praxis-Übungs-Projekt aus und dokumentieren Sie in Lernportfolio.

  ![Note](../x_res/note.png) Ablage im Lernportfolio (Scripte und Resultate)

## Stored Procedures

> ![Teacher](../x_res/Teacher.png) <br>

![Learn](../x_res/Learn.png)

Stored Procedures (auf Deutsch: gespeicherte Prozeduren) sind vordefinierte, in der Datenbank gespeicherte SQL-Abfolgen, die eine bestimmte Aufgabe oder eine Reihe von Aufgaben ausführen. Sie werden in der Regel verwendet, um die Wiederverwendbarkeit von SQL-Code zu erhöhen, komplexe logische Abläufe zu kapseln und die Performance zu verbessern, indem sie direkt in der Datenbank ausgeführt werden.

Eine Stored Procedure ist eine Sammlung von SQL-Befehlen, die auf der Datenbank gespeichert werden und bei Bedarf ausgeführt werden können. Sie ermöglicht es, wiederkehrende Operationen zu automatisieren und in einer zentralen Stelle zu verwalten.

Merkmale von Stored Procedures:
Wiederverwendbarkeit: Einmal erstellt, können Stored Procedures jederzeit wieder aufgerufen werden, ohne dass der SQL-Code jedes Mal neu geschrieben werden muss.
Zentralisierung der Logik: Geschäftslogik kann direkt in der Datenbank gespeichert werden, was zu einer zentralisierten Verwaltung führt.
Verbesserte Performance: Stored Procedures können optimiert werden, da sie auf der Datenbankebene ausgeführt werden und nicht über das Netzwerk gesendet werden müssen, was die Performance verbessern kann.
Sicherheit: Berechtigungen können so vergeben werden, dass Benutzer nur die gespeicherten Prozeduren aufrufen können, ohne direkten Zugriff auf die zugrunde liegenden Tabellen oder Daten zu haben.
Aufbau einer Stored Procedure:
In den meisten relationalen Datenbankmanagementsystemen (RDBMS) wie MySQL, SQL Server oder PostgreSQL sind Stored Procedures relativ ähnlich, aber die genaue Syntax kann je nach System variieren. Ein einfaches Beispiel für eine Stored Procedure in MySQL:

```
DELIMITER $$

CREATE PROCEDURE GetKundendaten (IN kunden_id INT)
BEGIN
    SELECT Name, Adresse, Telefon
    FROM Kunden
    WHERE KundenID = kunden_id;
END $$

DELIMITER ;
```
- Die Stored Procedure `GetKundendaten` wird erstellt, um Daten eines bestimmten Kunden abzurufen.
- Der Parameter `kunden_id` wird als Eingabe verwendet, um den spezifischen Kunden aus der Tabelle Kunden zu finden.
- Die `DELIMITER`-Anweisung wird verwendet, um ein anderes Trennzeichen für die SQL-Anweisungen zu setzen, da der `;`-Operator auch innerhalb der Prozedur verwendet wird.


Eine gespeicherte Prozedur wird mit der `CALL`-Anweisung aufgerufen:

```
CALL GetKundendaten(1);
```


### Auftrag 

![ToDo](../x_res/ToDo.png) *Zeit: ca. 120 Min*

![Learn](../x_res/Train_R1.png)

- beschreiben Sie das im Lernportfolio
- bauen Sie in ihre DB solche *stored procedure* ein, z.B. eine Abfrage über eine oder mehrere Tabellen oder verschiedene Berechnungen und/oder Zählungen mit SQL-Befehlen (MAX, AVG, ...)


![Note](../x_res/note.png) Ablage im Lernportfolio (Scripte und Resultate)

---

<br>

![Train](../x_res/Train_D1.png)

...

![Note](../x_res/note.png) Ablage im Lernportfolio (Scripte und Resultate)

---

![](../x_res/Buch.jpg)

## Referenzen

