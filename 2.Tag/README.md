![TBZ Logo](../x_res/tbz_logo.png)√
![m164 Picto](../x_res/m164_picto.jpg)



# m164 - Datenbanken erstellen und Daten einfügen

[TOC]

---

# Tag 2



> ![Teacher](../x_res/Teacher.png) <br> Recap / Q&A Tag 1 <br>
> [Lösung 1.Tag](1T_Loes.md)

## Generalisierung / Spezialisierung (Person mit der Rolle als Fahrer oder Disponent).

> ![Teacher](../x_res/Teacher.png) <br>  Story "Tourenplaner" und Problemlösung durch Generalisierung / Spezialisierung

![Learn](../x_res/Learn.png)

Die Datenbankmodellierung, die wir hier betrachten, beruht auf dem Attributkonzept. Wir definieren Attribute mit Attributsausprägungen und ordnen diese den Entitätstypen zu. Problematisch wird es, wenn jetzt mehrere Entitätstypen viele Attribute gemeinsam haben und einige nicht. Jetzt könnte **Redundanz** entstehen, wenn ein konkretes Objekt aus der realen Welt durch mehrere Entitätstypen beschrieben wird. In unserem konkreten Beispiel wären das Mitarbeiter, die auch als Kunden auftreten, oder Fahrer, die auch als Disponenten arbeiten. In Anlehnung an [ZEHNDER, Carl August (1989): Informationssysteme und Datenbanken] dürfen „lokale Attribute“ nur einmal in einer Datenbank auftreten, da sonst Redundanz vorliegt. Die Lösung des Problems besteht darin, dass die gemeinsamen Attribute in einem **allgemeinen Entitätstypen zusammengefasst** werden (Generalisierung). Die **nicht gemeinsamen Attribute** verbleiben in den Entitätstypen (Spezialisierung). Um einen Informationsverlust zu vermeiden, müssen die Datensätze der spezialisierten Tabellen per Fremdschlüssel auf die generalisierten Tabellen verweisen. Diese Beziehung wird auch als „**is_a**“-Beziehung bezeichnet: eine Person *ist ein* Fahrer.

Diese Beziehungsform ist auch in der objektorientierten Modellierung bekannt und wird dort mittels *Vererbung* gelöst.

![](./media/213e861ebd732e0ed4f44ca81930149a.png) 

Abbildung: Beispiel für Generalisierung/Speziaisierung

### Auftrag 

![ToDo](../x_res/ToDo.png) *Zeit: ca. 25 Min*

![Learn](../x_res/Train_R1.png)

  1. Erstellen Sie eine Zusammenfassung der Theorie in Ihrem Lernportfolio. Fügen Sie einen Screenshot mit zwei unterschiedlichen Beziehungen ein (z.B. Person <- MA, Person <- Kunde)
  2. Machen Sie ein eigenes Beispiel einer Genaralisierung.
  3. Sammeln Sie von anderen KlassenkameradInnen weitere Beispiele.
  4. Verwenden Sie [phind](www.phind.com) (oder ChatGPT), um Ansätze zur praktischen Umsetzung herauszufinden.

  ![Note](../x_res/note.png) Ablage im Lernportfolio (Scripte und Resultate)

---

## Beziehungsarten: Indentifying / Non-Identifying Relationship

> ![Teacher](../x_res/Teacher.png) <br> Lehrerinput: Person - Kleidung - Ausweis

![Learn](../x_res/Learn.png)

Es gibt verschiedene Beziehungsarten in Datenbanken. So unterscheidet man beispielsweise zwischen **Identifying** und **Non-Identifying Relationships** /Beziehungen.
Am folgenden Beispiel sollen die beiden Begriffe erläutert werden. Man geht davon aus, dass man zwei Tabellen hat. Die erste Tabelle ist die Parent-Tabelle (auch Eltern-, Master-Tabelle) und die zweite Tabelle ist die Child-Tabelle (auch Kind-, Detail-Tabelle).

Die Child-Tabelle hat grundsätzlich den Primary Key der Parent-Tabelle als Foreign Key. Je nachdem, wie sich nun der Primary Key der Child-Tabelle zusammensetzt, spricht man von einer Identifying oder einer Non-Identifying Relationship.

### Identifying Relationship

In diesen Beziehungen wird eine eindeutige Identifizierung der betreffenden Entitäten oder Individuen 
vorgenommen. Dies bedeutet, dass man klar und präzise sagen kann, wer oder was in der Beziehung steht. 
Diese Art von Beziehung lässt wenig Raum für Ambiguität und Missverständnisse. Beispielsweise ist die 
Beziehung zwischen einer Mutter und ihrem Kind eindeutig und spezifisch identifizierend.

Bei der Identifying Relationship ist der Foreign Key ein Teil des Primary Keys der Child-Tabelle:

![Identifying](./media/Identifying.png)

Bei der Identifying-Beziehung ist der Fremdschlüssel Bestandteil des Indentifikationsschlüssels. Die 
ID ist also eine aus mehreren (mindestens 2) Attributen zusammengesetzte Schlüsselkombination, und der 
Datensatz der referenzierenden Tabelle („unten“) identifiziert sich teilweise durch den Datensatz, auf 
den er verweist. Ein Raum kann in der ID also unter anderem den Fremdschlüssel auf das Gebäude haben. 
Eine non-identifying Beziehung wäre hier nicht so sinnvoll, denn darin könnte ich jederzeit den 
Fremdschlüsselwert ändern und den Raum einem anderen Gebäude zuordnen.

<br> 

### Non-Identifying Relationship

Nicht-identifizierende Beziehungen sind weniger spezifisch (=genau) und können verschiedene Entitäten oder 
Individuen umfassen. Diese Beziehungen sind oft allgemeiner und vager. Zum Beispiel ist die Beziehung eines 
Lehrers zu "einer Gruppe von Schülern" eine nicht-identifizierende Beziehung, da nicht genau gesagt wird, 
welche spezifischen Schüler gemeint sind.

Bei der nicht-identifizierende Beziehung ist der Fremdschlüssel kein (!) Teil des Primärschlüssels der 
abhängigen Kind-Tabelle.

![Non-Identifying](./media/Non-Identifying.png)

Die Beziehung Mitarbeiter/Abteilung ist da ein anderes Beispiel für eine non-identifying-Beziehung. 
Der Fremdschlüssel auf den Arbeitgeber gehört nicht zur Identität einer Person, weil der Inhalt 
wechseln könnte und die Beziehung immer noch stimmt (also die Firma, auf die referenziert wird).



### Auftrag 

![ToDo](../x_res/ToDo.png) *Zeit: ca. 25 Min*

![Learn](../x_res/Train_R1.png)

  1. Erstellen Sie eine Zusammenfassung der Theorie in Ihrem Lernportfolio. Fügen Sie einen Screenshot des Beispiels mit zwei unterschiedlichen Beziehungen <br> (z.B. Person -- Kleidungsstücke, Person - Ausweis)
  2. Suchen Sie ein eigenes Beispiel, wo es Sinn macht, eine "Identifying" Beziehung zu verwenden.
  3. Sammeln Sie von anderen KlassenkollegInnen weitere Beispiele und notieren Sie diese im Lernportfolio.
4. Verwenden Sie [phind](www.phind.com) (oder ChatGPT), um Anwendungsfälle von Indetifiying Relationship zu finden?

![Note](../x_res/note.png) Ablage im Lernportfolio (Scripte und Resultate)

---

<br>

---

# DBMS (Datenbank Management System)

![Learn](../x_res/Learn.png)

Ein **Datenbanksystem** (**DBS**) ist ein System zur elektronischen Datenverwaltung. Die wesentliche Aufgabe eines DBS ist es, große Datenmengen effizient, widerspruchsfrei und dauerhaft zu speichern und benötigte Teilmengen in unterschiedlichen, bedarfsgerechten Darstellungsformen für Benutzer und Anwendungsprogramme bereitzustellen.

Ein DBS besteht aus zwei Teilen: der Verwaltungssoftware, genannt **Datenbankmanagementsystem** (DBMS) und der Menge der zu verwaltenden Daten, der eigentlichen **Datenbank** (DB). Die Verwaltungssoftware organisiert intern die strukturierte Speicherung der Daten und kontrolliert alle lesenden und schreibenden Zugriffe auf die Datenbank. Zur Abfrage und Verwaltung der Daten bietet ein Datenbanksystem eine Datenbanksprache an.

Datenbanksysteme gibt es in verschiedenen Formen. Die Art und Weise, wie ein solches System Daten speichert und verwaltet, wird durch das Datenbankmodell festgelegt. Die bekannteste Form eines Datenbanksystems ist das **Relationale Datenbanksystem**.

![Datenbank - Was ist eine Datenbank? \| Datenbank Grundlagen](./media/Aufbau_DBMS.png)

## Merkmale eines DBMS

Gemäss seiner Definition muss ein DBMS folgende Funktionalitäten bieten:

**Integrierte Datenhaltung.**  Das DBMS ermöglicht die einheitliche Verwaltung aller von den Anwendungen benötigten Daten. Somit wird jedes logische Datenelement, wie beispielsweise der Name eines Kunden, an nur einer Stelle in der Datenbank gespeichert. Dabei muss ein DBMS die Möglichkeit bieten, eine Vielzahl komplexer Beziehungen zwischen den Daten zu definieren sowie zusammenhängende Daten schnell und effizient miteinander zu verknüpfen. In manchen Fällen kann eine kontrollierte Redundanz allerdings nützlich sein, um die Effizienz der Verarbeitung von Anfragen zu verbessern.

**Sprache.** Das DBMS stellt an seiner Schnittstelle eine Datenbanksprache (*query language*) für die folgenden Zwecke zur Verfügung:

-   Datenanfrage (Data Query/Retrieval Language, DQL / DRL &rarr; `SELECT` ),
-   Verwaltung der Datenstruktur (Data Definition Language, DDL &rarr; `CREATE` ),
-   Datenmanipulation (Data Manipulation Language, DML &rarr; `INSERT` ),
-   Berechtigungssteuerung (Data Control Language, DCL &rarr; `GRANT`),
-   Transaktionen (Transaction Control Language, TCL &rarr; `COMMIT` ).

  Siehe auch [MySQL](https://dev.mysql.com/doc/refman/8.0/en/sql-statements.html), [MariaDB](https://mariadb.com/kb/en/sql-statements/)

Da viele verschiedene Benutzer mit unterschiedlichen technischen Kenntnissen und Anforderungen auf eine Datenbank zugreifen, sollte ein DBMS eine Vielzahl von Benutzeroberflächen bereitstellen. Dazu zählen Anfragesprachen für gelegentliche Benutzer, Programmierschnittstellen und grafische Benutzeroberflächen (Graphical User Interface, GUI). Die Möglichkeit eines Webzugriffs ist heute eine Grundvoraussetzung für den Einsatz eines DBMS.

**Katalog.** Der Katalog (*data dictionary*) ermöglicht Zugriffe auf die Datenbeschreibungen der Datenbank, die auch als Metadaten bezeichnet werden.

![Aufbau](./media/aa955734404b630b9214a2e7d7868173.png)

Abbildung: data dictionary

**Benutzersichten.** Für unterschiedliche Klassen von Benutzern sind verschiedene Sichten (*views*) erforderlich, die bestimmte Ausschnitte aus dem Datenbestand beinhalten oder diesen in einer für die jeweilige Anwendung benötigten Weise strukturieren. Die Sichten sind im externen Schema der Datenbank definiert.

**Konsistenzkontrolle.**  Die Konsistenzkontrolle, auch als Integritätssicherung bezeichnet, übernimmt die Gewährleistung der Korrektheit von Datenbankinhalten und der korrekten Ausführung von Änderungen. Ein korrekter Datenbankzustand wird durch benutzerdefinierte Integritätsbedingungen (*constraints*) im DBMS definiert, die während der Laufzeit der Anwendungen vom System kontrolliert werden. Daneben wird aber auch die physische Integrität sichergestellt, d. h. die Gewährleistung intakter Speicherstrukturen und Inhalte (Speicherkonsistenz).

**Datenzugriffskontrolle.** Durch die Festlegung von Regeln kann der unautorisierte Zugriff auf die in der Datenbank gespeicherten Daten verhindert werden (*access control*). Dabei kann es sich um personenbezogene Daten handeln, die datenschutzrechtlich relevant sind, oder um firmenspezifische Datenbestände. Rechte lassen sich auch auf Sichten definieren.

**Transaktionen**. Mehrere Datenbankänderungen, die logisch eine Einheit bilden, lassen sich zu Transaktionen zusammenfassen, die als Ganzes ausgeführt werden sollen (*Atomarität*) und deren Effekt bei Erfolg permanent in der Datenbank gespeichert werden soll (*Dauerhaftigkeit*).

**Mehrbenutzerfähigkeit.** Konkurrierende Transaktionen mehrerer Benutzer müssen synchronisiert werden, um gegenseitige Beeinflussung, z. B. bei Schreibkonflikten auf gemeinsam genutzten Daten, zu vermeiden (concurrency control). Dem Benutzer erscheinen die Daten so, als ob nur eine Anwendung darauf zugreift (Isolation).

**Datensicherung.** Das DBMS muss in der Lage sein, bei auftretenden Hard- oder Softwarefehlern wieder einen korrekten Datenbankzustand herzustellen (recovery).

## Vorteile des Datenbankeinsatzes

Zusätzlich zu den genannten Merkmalen ergibt sich bei Einsatz eines Datenbanksystems eine Reihe von Vorteilen:

**Nutzung von Standards.** Der Einsatz einer Datenbank erleichtert die Einführung und Umsetzung zentraler Standards in der Datenorganisation. Dies umfasst Namen und Formate von Datenelementen, Schlüssel, Fachbegriffe.

**Effizienter Datenzugriff.** Ein DBMS braucht eine Vielzahl komplizierter Techniken zum effizienten Speichern und Wiederauslesen (retrieval) großer Mengen von Daten.

**Kürzere Softwareentwicklungszeiten.** Ein DBMS bietet viele wichtige Funktionen, die allen Anwendungen, die auf eine Datenbank zugreifen wollen, gemeinsam sind. In Verbindung mit den angebotenen Datenbanksprachen wird somit eine schnellere Anwendungsentwicklung ermöglicht, da der Programmierer von vielen Routineaufgaben entlastet wird.

**Hohe Flexibilität.**  Die Struktur der Datenbank kann bei sich ändernden Anforderungen ohne große Konsequenzen für die bestehenden Daten und die vorhandenen Anwendungen modifiziert werden (Datenunabhängigkeit)

**Hohe Verfügbarkeit.** Viele transaktionsintensive Anwendungen (z. B. Reservierungssysteme, Online-Banking) haben hohe Verfügbarkeitsanforderungen. Ein DBMS stellt die Datenbank allen Benutzern dank der Synchronisationseigenschaften **gleichzeitig** zur Verfügung. Änderungen werden nach Transaktionsende sofort sichtbar.

**Grosse Wirtschaftlichkeit.** Die durch den Einsatz eines DBMS erzwungene Zentralisierung in einem Unternehmen erlaubt die Investition in leistungsstärkere Hardware, statt jede Abteilung mit einem eigenen (schwächeren) Rechner auszustatten. Somit reduziert der Einsatz eines DBMS die Betriebs- und Verwaltungskosten.

## Nachteile von Datenbanksystemen

Trotz der Vorteile eines DBMS gibt es auch Situationen, in denen ein solches System unnötig hohe Zusatzkosten im Vergleich zur traditionellen Dateiverarbeitung mit sich bringen würde:

-   Hohe Anfangsinvestitionen für Hardware und Datenbanksoftware.
-   Ein DBMS ist Allzweck-Software, somit weniger effizient für spezialisierte Anwendungen.
-   Bei konkurrierenden Anforderungen kann das Datenbanksystem nur für einen Teil der Anwendungsprogramme optimiert werden.
-   Mehrkosten für die Bereitstellung von Datensicherheit, Mehrbenutzer- Synchronisation und Konsistenzkontrolle.
-   Hochqualifiziertes Personal erforderlich, z. B. Datenbankdesigner, Datenbankadministrator.
-   Verwundbarkeit durch Zentralisierung (Ausweg: Verteilung).

Unter bestimmten Umständen ist der Einsatz regulärer Dateien sinnvoll:

-   Ein gleichzeitiger Zugriff auf die Daten durch mehrere Benutzer ist nicht erforderlich.
-   Für die Anwendung bestehen feste Echtzeitanforderungen, die von einem DBMS nicht ohne Weiteres erfüllt werden können.
-   Es handelt sich um Daten und Anwendungen, die einfach und wohldefiniert sind und keinen Änderungen unterliegen werden.

## Produkte

| DBMS          | \$ ?  | Hersteller   | Modell/Charakteristik                                         |
|---------------|-------|--------------|---------------------------------------------------------------|
| Adabas        | \$    | Software AG  | NF2 -Modell (nicht normalisiert)                              |
| Cache         | \$    | InterSystems | hierarchisch, “postrelational”                                |
| DB2           | \$    | IBM          | objektrelational                                              |
| Firebird      |       | –            | relational, basierend auf InterBase                           |
| IMS           | \$    | IBM          | hierarchisch, Mainframe – DBMS                                |
| Informix      | \$    | IBM          | objektrelational                                              |
| InterBase     | \$    | Borland      | relational                                                    |
| MS Access     | \$    | Microsoft    | relational, Desktop – System                                  |
| MS SQL Server | \$    | Microsoft    | objektrelational                                              |
| MySQL         |       | MySQL AB     | relational                                                    |
| Oracle        | \$    | ORACLE       | objektrelational                                              |
| PostgreSQL    |       | –            | objektrelational, hervorgegangen aus Ingres und Postgres      |
| Sybase ASE    | \$    | Sybase       | relational                                                    |
| Versant       | \$    | Versant      | objektorientiert                                              |
| Visual FoxPro | \$    | Microsoft    | relational, Desktop – System                                  |
| Teradata      | \$    | NCR Teradata | relationales Hochleistungs DBMS, speziell für Data Warehouses |


### Auftrag


![ToDo](../x_res/ToDo.png) *Zeit: 25Min, 2er Team*

![Learn](../x_res/Train_R1.png)

Lesen Sie obige Theorie aufmerksam durch und lösen Sie dazu folgende Aufgaben: 

1.	![Note](../x_res/note.png) Erstellen Sie eine Zusammenfassung in ihrem Lernportfolio zur Theorie DBMS. Unklare Begriffe sollten Sie im Internet kurz nachschlagen oder die Lehrperson fragen. 

2.	DB-Engine-Produkte:
    -	Vergleiche Sie die obige Liste mit der aktuellen DB-Ranking: [DB-Engine Ranking](https://db-engines.com/en/ranking)
    -	![Note](../x_res/note.png) Nehmen Sie die wichtigsten 10 DB-Engines in ein **Mindmap** auf
  
---

<br>

---

# DDL - Anlegen einer Datenbank (Repetition ÜK Modul 106)

---

>> ![](../x_res/Wichtig.png)  **WICHTIG: Fall Sie den ÜK 106 noch nicht hatten, müssen Sie zusätzliche Zeit zu Hause einplanen, um all die SQL-Befehle kennen und benutzen zu lernen! Bei jeder "Repetition" wird es Links mit den nötigen Selbstlernunterlagen geben. Beanspruchen Sie in dem Fall unbedingt auch die Unterstützung der Lehrperson!**

---

![Learn](../x_res/Learn.png)

In mySQL ist «Schema» ein Synonym für «Datenbank». In einem DBS können Sie mehrere Schema’s anlegen. Das macht auch Sinn, da man damit die Daten besser strukturieren und gruppieren kann und somit mehr Datenordnung ins DBS reinbringt.

Zusammenfassend kann man sagen: Ein DBS ist u.a. eine Sammlung aus Datenbanken und eine Datenbank ist eine Sammlung aus mehreren Tabellen.

![Ein Bild, das Text enthält. Automatisch generierte Beschreibung](./media/0dc1457bc708b9931982e42166ff8901.png)

Referenzdokumentation finden Sie hier: [MySQL](https://dev.mysql.com/doc/refman/8.0/en/create-database.html), [MariaDB](https://mariadb.com/kb/en/create-table/)

Wichtig ist, dass Sie den Charakter Set gut überlegt festlegen. Beim Schema bzw. Datenbank kann er als Default Wert definiert werden. Es gibt aber die Möglichkeit, untergeordnet auf Tabellen- oder Spaltenebene zu definieren. Das bedeutet, dass der Charakter Set untergeordnet übersteuert wird.

![Ein Bild, das Text enthält. Automatisch generierte Beschreibung](./media/bb2f54775e9f604ec6e71a0d66c1c644.png)


## Definition Character Set (CHARSET=)

Im Modul [162](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Daten_Formate/Zeichencodierung.md) (und Modul **114**) haben wir Zeichenkodierungen kennengelernt, darum hier nur eine Zusammenfassung. Zeichensatzkodierungen sind Tabellen, die bestimmten Byte-Werten konkrete Zeichen zuordnen:

-   **ASCII** (Veraltet, American Standard Code for Information Interchange): <br>   Ist 7-Bit codiert, hat ca. 32 Steuerzeichen und 96 druckbare Zeichen, ist auf die englische Sprache ausgerichtet.  
-   **ANSI** (ISO-8859): Der Zeichensatz [ISO-8859-1](https://de.wikipedia.org/wiki/ISO_8859-1) (**Latin1**) ist der erste von insgesamt 16 ländereigenen Zeichensätzen und ist für Westeuropa gemacht, beinhaltet also z.B. deutsche, französische oder skandinavische Sonderzeichen. ANSI deckt (fast) alle ASCII Zeichen (mit demselben Code) ab und codiert, mit dem höchstwertigen Bit (8.Bit) gesetzt (>=80h), weitere 96 internationale Zeichen via wählbare Codepages.
-   **Unicode** (Universal Character Set, UCS): Entstanden Ende der 80er Jahre mit dem Ziel alle Sprachen der Welt in einem Zeichensatz zu vereinen, ist der Unicode der grösste und umfassendste Zeichensatz. Durch die 32-Bit-Speichergrösse pro Zeichen kann Unicode ca. 100'000 verschiedene Zeichen einem eindeuigen Code zuordnen. 
- **UTF-8** (Standard, UCS Transformation Format 8-Bit): UTF-8 ist die häufigste verwendete Kodierung für Unicode-Zeichen mit hohem ASCII-Anteil – die ersten 128 Zeichen entsprechen der ASCII-Tabelle. Aktuell sind über 95% aller Websites mit UTF-8 codiert. Siehe [History](https://w3techs.com/technologies/history_overview/character_encoding) <br> Der Vorteil von UTF-8 liegt in der [angepassten Speichergrösse](https://de.wikipedia.org/wiki/UTF-8#Algorithmus) eines einzelenen Zeichens, das von einem Byte (ASCII) bis zu vier Bytes reicht (Emoji): in 8-Bit Sprüngen.
- **UTF-16, UTF-32**: Gegenüber UTF-8 ist die angepasste Speichergrösse bei UTF-16 16 Bit (&#8594; Java), und bei UTF-32 fix 32 Bit, was einen grossen Speicherbedarf bei den häufigsten Zeichen (ASCII) bedeutet!


--

### Auftrag (Repetition M106)

![ToDo](../x_res/ToDo.png) *Zeit: 25Min, 2er Team*

![Learn](../x_res/Train_R1.png)

> Für diejenigen, die den ÜK noch nicht hatten: [Create Präsentation](./create-table.pdf) und [DDL_Befehle](./DDL_Intro.md)


![Note](../x_res/note.png)  Benutzen Sie für folgende Aufgaben z.B. den internen Editor von Workbench und speichern Sie die erstellten Scripte im Lernportfolio ab.

![Editor](./media/SQL-Script_WB.png)

- **CREATE SCHEMA, CREATE TABLE**: Erstellen Sie die folgenden zwei Tabellen mit einem SQL-Script. Benutzen Sie `utf8mb4` als Default Charset. <br> ![Disp-Fahrer](./media/TP_Disp_Fahrer.png)
  - **DROP TABLE**: Löschen Sie wieder eine der beiden Tabellen. Danach stellen Sie die Tabelle mit dem vorher erstellten Script wieder her.
  - **Generalisieren**: Erstellen Sie eine neue Tabelle `tbl_Mitarbeiter` und fügen Sie folgende Attribute ein:
  
	| Attribut     | Datentyp    |
	|--------------|-------------|
	| MA_ID        | INT         |
	| Name         | VARCHAR(50) |
	| Vorname      | VARCHAR(30) |
	| Geburtsdatum | DATETIME    |
	| Telefonnummer | VARCHAR(12)|
	| Einkommen    | FLOAT(10,2) |

  - **ALTER TABLE MODIFY/CHANGE**: Ändern Sie den Charset von `Name` und `Vorname` auf `latin1`.
  - **ALTER TABLE DROP COLUMN**: Löschen Sie aus den Tabellen `tbl_fahrer` und `tbl_disponent` die Attribute `Name`, `Vorname` und `Telefonnummer`.
  - **Spezialisierung** - **ALTER TABLE ADD COLUMN**: Erstellen Sie die notwendigen *Fremdschlüssel*, um die Spezialisiersungen zu erstellen!
 
![Note](../x_res/note.png)  *Legen Sie das erstellte Scripts bitte in ihr Lernportfolio!*

---

## Forward Engineering mit Workbench

![Learn](../x_res/Train_D1.png)

## Forward Engineering

Forward Engineering bezieht sich auf den Prozess der Erstellung einer Datenbank aus einem physischen Datenmodell. Das Datenmodell kann in einem Diagrammformat vorliegen, das die Tabellen, Spalten, Beziehungen und Einschränkungen der Datenbank beschreibt.

Der Vorteil von Forward Engineering besteht darin, dass es den Prozess der Datenbankerstellung automatisiert und Fehler reduziert. Indem das Datenmodell zuerst erstellt und dann in eine Datenbank umgewandelt wird, können Probleme und Inkonsistenzen frühzeitig erkannt werden, bevor die Datenbank im Einsatz ist.

![DDL+DQL](./media/DDL_DML.png)

Aus dem bestehenden physischen Modell (z.B: Tourenplaner) erstellt man in Mysql-Workbench unter **Database > Forward Engineer...** direkt ein **DDL-SQL-Script**, welches die Struktur der Datenbasis aus dem Model erzeugt. 

![ToDo](../x_res/ToDo.png) **Aufgaben**

Erzeugen Sie ein Model in welchem Sie einige Relationen und Tabellen haben, mit verschiedenen Attributen (oder nehmen Sie Tourenplaner).
Dann sollten Sie mit Forward-Engineering die SQL-Statements erzeugen lassen und diese als Vorlage sichern.

**Forward Engineering** wählen:

![](./media/03_forward_engineering_1.png)

Als nächstes die **IP-Adresse** des Datenbank-Servers eingeben (z.b. TBZ-Cloudserver, localhost (127.0.0.1)  oder AWS Instanz). Benutzernamen und Passwort haben Sie entweder selbst gesetzt oder wird Ihnen von der Lehrperson mitgeteilt.

![](./media/03_forward_engineering_2.png)

Dann wählen, welche **Statements** man generieren lassen will: *(Anzeige kann je nach Version variieren!)*

![](./media/03_forward_engineering_3.png)

Ggf. **filtern**, was man alles nicht erzeugen lassen will:

![](./media/03_forward_engineering_4.png)

Ein SQL-Skript wird generiert, aus welchem man die Statements "**erlernen**" kann:

![](./media/03_forward_engineering_5.png)


**Save to file ...** Diese Statements als "Vorlage" irgendwo wegsichern, am besten in das Lernportfolio.

Mit **Next** wird das SQL-Script dem DB-Server übermittelt. Verbinden Sie sich nun mit dem DB-Server und lassen Sie sich die neu erstellte Datenstruktur anzeigen:

![](./media/03_forward_engineering_6.png)
 

- Untersuchen Sie die Strukturen, die Sie erzeugt haben.
- Untersuchen Sie das SQL-Script. Welche Befehle(s-Teile) sind neu für Sie. 

![Note](../x_res/note.png) Machen Sie einen Eintrag ins Lernportfolio.


### Synchronize Modell

Mit **Synchronize Modell** kann man das Modell mit dem erzeugten DB-Schema vergleichen und synchronisieren. Dabei werden die Definitionen von Tabellen und Relationen im Modell mit einer laufenden Datenbank verglichen und allenfalls synchronisiert.  

![Achtung](../x_res/caution.png) **Achtung** der Datenbank- oder Schema-Name hängt vom **Physical Schema Name** ab, im Beispiel hier `mydb`
![](./media/03_sync_modell_1.png)

Im Fall man dies ändern möchte, kann man mit rechter Maustaste auf dem Namen klicken und dann `Edit Schema` wählen:
![](./media/03_sync_modell_2.png)

Danach kann man Unter `Database -> Synchronize Modell` die Datenbank erzeugt, beziehungsweise angepasst werden.

Damit Sie ein bisschen ein Gefühl für die Möglichkeiten kriegen können ...

1. Erzeugen Sie im oben erstellten Modell eine zusätzliche Tabelle (z.B. tbl_MA) und mind. eine Relation zwischen 2 bestehenden Tabellen.
2. Erzeugen Sie ein zusätzliches Attribut in einer bestehenden Tabelle.
3. Synchronisieren Sie das Modell und schauen Sie das geänderte Schema an.

### Non-/Identifying Relationship

- Erstellen Sie ein neues Script mit Forward Engineering aus dem Schema **Non-/Identifying Relationship** (Person - Kleidungsstück / - Ausweis). Untersuchen Sie die Unterschiede der Beziehungen im SQL-Script (PRIMARY KEY)! 

![Note](../x_res/note.png) Legen Sie die beiden **beziehungsbestimmenden SQL-Befehle** (zu Kleidungsstück, und zuAusweis) mit einem entsprechenden Kommentar im Lernportfolio ab!


---

### Auftrag für Fortgeschrittene

![ToDo](../x_res/ToDo.png) *Zeit: 25Min, Einzelarbeit*

![Learn](../x_res/Train_D1.png)


**Recherche und Zusammenfassung**

Recherchieren Sie folgende Begriffe im Internet und erstellen Sie eine Zusammenfassung im Lernportfolio.


1. *Partition* (bezgl. Datenbanken)
2. Was macht eine *storage engine* in einer Datenbank?
3. Was ist ein *Tablespace* (InnoDB)? Wie sieht die *Tablespace Architektur* aus? [Diagramm](./media/Tablespace.png)

![Note](../x_res/note.png) Ablage im Lernportfolio (Scripte und Resultate)

---


![Checkpoint](../x_res/CP.png)

#### Checkpoint

- Generalisierung/Spezialisierung: Warum macht man das?
- Identifying Beziehung! Warum macht man das?
- Wie wird eine Identifying Beziehung mit SQL-Befehl(en) erstellt? 
- Welche Befehle gehören zur DDL-Gruppe?


---

![](../x_res/Buch.jpg)

## Referenzen

[MySQL](https://dev.mysql.com/doc/refman/8.0/en/sql-statements.html)

[MariaDB](https://mariadb.com/kb/en/sql-statements/)

[DB-Engine Ranking](https://db-engines.com/en/ranking)